/* global afterEach, beforeEach, describe, it */
'use strict';

const TransformDataStream = require('../../src/lib/util/transform-data-stream'),
    expect = require('chai').expect,
    sinon = require('sinon');

describe('transform-data-stream', () => {
  let pushSpy,
      stream;

  const line = '23,-161,0.005,0.0025,0.005';
  const buffer = Buffer.from(line, 'utf8');
  const options = {
    csvColumns: [
      'LATITUDE',
      'LONGITUDE',
      'MAPPED_PGAD',
      'MAPPED_S1D',
      'MAPPED_SSD'
    ],
    saValues: ['MAPPED_SSD', 'MAPPED_S1D'],
    tableDef: [
      'latitude NUMERIC',
      'longitude NUMERIC',
      'scalar NUMERIC',
      'vector NUMERIC ARRAY'
    ]
  };

  afterEach(() => {
    stream.destroy();
    pushSpy.restore();
  });

  beforeEach(() => {
    stream = TransformDataStream(options);
    pushSpy = sinon.stub(stream, 'push').callsFake(() => {
      return;
    });
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof TransformDataStream).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(TransformDataStream).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(() => {
        const transformStream = TransformDataStream();
        transformStream.destroy();
      }).to.not.throw(Error);
    });
  });

  describe('_flush', () => {
    it('pushes the partial row on to the stream', () => {
      let spy = sinon.spy();

      sinon.stub(stream, 'parseLine').callsFake(() => {
        return '';
      });

      stream._flush(spy);
      expect(spy.called).to.be.equal(true);
      expect(stream.parseLine.called).to.be.equal(true);
      expect(pushSpy.called).to.be.equal(true);
    });
  });

  describe('_transform', () => {
    it('pushes parsed chunk onto the stream', () => {
      let spy = sinon.spy();

      sinon.stub(stream, 'parseChunk').callsFake(() => {
        return '';
      });

      stream._transform(buffer, null, spy);
      expect(spy.called).to.be.equal(true);
      expect(stream.parseChunk.called).to.be.equal(true);
      expect(pushSpy.called).to.be.equal(true);
    });
  });

  describe('isGoodLine', () => {
    it('checks to make sure line is parsable', () => {
      let result;
      result = stream.isGoodLine('1111');
      expect(result).to.be.equal(false);
      result = stream.isGoodLine('1,2,3,4');
      expect(result).to.be.equal(true);
    });
  });

  describe('parseChunk', () => {
    it('parse chunk into parsed lines', () => {
      sinon.stub(stream, 'isGoodLine').callsFake(() => {
        return true;
      });
      sinon.stub(stream, 'parseLine').callsFake(() => {
        return;
      });
      stream.parseChunk('\n');
      expect(stream.isGoodLine.called).to.be.equal(true);
      expect(stream.parseLine.called).to.be.equal(true);
    });
  });

  describe('parseLine', () => {
    it('parse a single record', () => {
      const saValues = '"{0.005,0.0025}"';
      const parsedLine = '23,-161,0.005,' + saValues;
      sinon.stub(stream, 'parseSpectalPeriodArray').callsFake(() => {
        return saValues;
      });
      const result = stream.parseLine(line);
      expect(stream.parseSpectalPeriodArray.called).to.be.equal(true);
      expect(stream.parseSpectalPeriodArray.getCall(0).args[0]).to.deep.equal(line);
      expect(result).to.be.equal(parsedLine);
    });
  });

  describe('parseSpectalPeriodArray', () => {
    it('parse the spectral values for a single record', () => {
      const saValues = '"{0.005,0.0025}"';
      const result = stream.parseSpectalPeriodArray(line);
      expect(result).to.be.equal(saValues);
    });
  });
});
