/* global after, before, describe, it */
'use strict';


const expect = require('chai').expect,
    Config = require('../../src/lib/util/config'),
    fs = require('fs'),
    sinon = require('sinon');

describe('util/config', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof Config).to.equal('function');
    });

    it('can be instantiated', () => {
      expect(Config).to.not.throw(Error);
    });
  });

  describe('backportParams', () => {
    it('replaces old parameters', () => {
      const obj = {
        DB_HOST: 'localhost',
        DB_USER: 'user',
        DB_PASSWORD: 'password',
      };
      const oldObj = {
        database: 'localhost',
        pgsql_read_only_user: 'user',
        pgsql_read_only_password: 'password',
      };
      const config = new Config();
      expect(config.backportParams(oldObj)).to.deep.equal(obj);
    });
  });

  describe('extend', () => {
    it('extends the config object', () => {
      const config = new Config();
      config.extend({testing: true});
      expect(config.config.testing).to.equal(true);
    });
  });

  describe('get', () => {
    it('gets the parameter from the config', () => {
      const config = new Config();
      config.extend({testing: true});
      expect(config.get('testing')).to.equal(true);
    });
  });

  describe('getDefaults', () => {
    it('sets default values based on objet', () => {
      const config = new Config();
      const obj = config.getDefaults([
        {
          name: 'question1',
          default: 'answer1'
        },
        {
          name: 'question2',
          default: 'answer2'
        }
      ]);
      expect(obj.question1).to.equal('answer1');
      expect(obj.question2).to.equal('answer2');
    });
  });

  describe('getSupplemental', () => {
    let stub1;
    let stub2;

    after(() => {
      stub1.restore();
      stub2.restore();
    });

    before(() => {
      stub1 = sinon.stub(fs, 'readFileSync').callsFake(() => {
        return '{}';
      });
      stub2 = sinon.stub(fs, 'existsSync').callsFake(() => {
        return true;
      });
    });

    it('get version, revision, and ', () => {
      const config = new Config();
      const result = config.getSupplemental({});

      expect(fs.readFileSync.called).to.equal(true);
      expect(result.hasOwnProperty('VERSION')).to.equal(true);
      expect(result.hasOwnProperty('REVISION')).to.equal(true);
      expect(result.hasOwnProperty('webDir')).to.equal(true);
    });
  });

  describe('set/unset', () => {
    it('add and remove key value pair to config', () => {
      const obj = {
        key: 'key',
        value: 'value'
      };
      const config = new Config({});
      config.set(obj.key, obj.value);
      expect(config.config[obj.key]).to.equal(obj.value);
      config.unset(obj.key);
      expect(config.config[obj.key]).to.be.undefined;
    });
  });
});