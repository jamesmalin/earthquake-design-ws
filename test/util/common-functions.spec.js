/* global before, describe, it */
'use strict';

const expect = require('chai').expect;

const CommonFunctions = require('../../src/lib/util/common-functions');

describe('CommonFunctions', () => {
  describe('getPeriodValue', () => {
    let DATA;

    before(() => {
      DATA = {
        periods: [0, 1, 2, 3, 4, 5],
        values: [0, 1, 2, 3, 4, 5]
      };
    });

    it('returns expected results', () => {
      expect(CommonFunctions.getPeriodValue(DATA, 0)).to.equal(0);
      expect(CommonFunctions.getPeriodValue(DATA, 1)).to.equal(1);
      expect(CommonFunctions.getPeriodValue(DATA, 2)).to.equal(2);
      expect(CommonFunctions.getPeriodValue(DATA, 3)).to.equal(3);
      expect(CommonFunctions.getPeriodValue(DATA, 4)).to.equal(4);
      expect(CommonFunctions.getPeriodValue(DATA, 5)).to.equal(5);
    });

    it('returns null when appropriate', () => {
      expect(CommonFunctions.getPeriodValue(DATA, 0.5)).to.equal(null);
      expect(CommonFunctions.getPeriodValue(DATA, -1)).to.equal(null);
      expect(CommonFunctions.getPeriodValue(DATA, 5.5)).to.equal(null);
    });
  });
});
