/* global describe, it */
'use strict';

const NSHM2014Factory = require('../../src/lib/basis/nshm_2014-factory'),
    expect = require('chai').expect,
    sinon = require('sinon');

const _DUMMY_FACTORY = {
  metadataService: {
    getData: () => {
      return Promise.resolve([]);
    }
  },
  uniformHazardService: {
    getData: () => {
      return Promise.resolve({});
    }
  },
  deterministicService: {
    getData: () => {
      return Promise.resolve({});
    }
  },
  riskCoefficientService: {
    getData: () => {
      return Promise.resolve({});
    }
  },
  tSubLService: {
    getData: () => {
      return Promise.resolve({ response: { data: {} } });
    }
  },
  siteAmplificationService: {
    getData: () => {
      return Promise.resolve({ response: { data: {} } });
    }
  },
  designCategoryFactory: {
    getDesignCategory: () => {
      return Promise.resolve([]);
    }
  },
  spectraFactory: {
    getSpectrum: () => {
      return Promise.resolve([]);
    },
    getVerticalSpectrum: () => {
      return Promise.resolve([]);
    }
  },
  verticalCoefficientFactory: {
    getVerticalCoefficientData: () => {
      return Promise.resolve({
        cv: ''
      });
    }
  }
};

describe('nshm-2014-factory', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof NSHM2014Factory).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(NSHM2014Factory).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(() => {
        const factory = NSHM2014Factory();
        factory.destroy();
      }).to.not.throw(Error);
    });

    it('Sets the correct referenceDocument value', () => {
      const factory = NSHM2014Factory({
        referenceDocument: 'nshm-2014'
      });
      expect(factory.referenceDocument).to.equal('nshm-2014');
    });
  });

  describe('get', () => {
    it('returns a promise', () => {
      var factory;

      factory = NSHM2014Factory(_DUMMY_FACTORY);

      expect(factory.get()).to.be.instanceof(Promise);

      factory.destroy();
    });

    it('calls expected sub-methods', done => {
      const factory = NSHM2014Factory(_DUMMY_FACTORY);

      sinon.spy(factory.spectraFactory, 'getVerticalSpectrum');
      sinon.spy(factory.verticalCoefficientFactory, 'getVerticalCoefficientData');

      sinon.spy(factory.metadataService, 'getData');
      sinon.spy(factory.uniformHazardService, 'getData');
      sinon.spy(factory.deterministicService, 'getData');
      sinon.spy(factory.riskCoefficientService, 'getData');

      sinon.stub(factory, 'computeBasicDesign').callsFake(() => {
        return Promise.resolve({});
      });

      sinon.spy(factory.siteAmplificationService, 'getData');
      sinon.stub(factory, 'computeFinalDesign').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.spy(factory.designCategoryFactory, 'getDesignCategory');
      sinon.stub(factory, 'computeSpectra').callsFake(() => {
        return Promise.resolve([]);
      });

      factory
        .get({})
        .then((/*result*/) => {
          expect(factory.spectraFactory.getVerticalSpectrum.callCount).to.equal(1);
          expect(factory.verticalCoefficientFactory.getVerticalCoefficientData.callCount).to.equal(1);

          expect(factory.metadataService.getData.callCount).to.equal(1);
          expect(factory.uniformHazardService.getData.callCount).to.equal(1);
          expect(factory.deterministicService.getData.callCount).to.equal(1);
          expect(factory.riskCoefficientService.getData.callCount).to.equal(1);

          expect(factory.computeBasicDesign.callCount).to.equal(1);

          expect(factory.siteAmplificationService.getData.callCount).to.equal(
              1
          );
          expect(factory.computeFinalDesign.callCount).to.equal(1);
          expect(
              factory.designCategoryFactory.getDesignCategory.callCount
          ).to.equal(1);
          expect(factory.computeSpectra.callCount).to.equal(1);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          factory.destroy();
          done(err);
        });
    });
  });
});
