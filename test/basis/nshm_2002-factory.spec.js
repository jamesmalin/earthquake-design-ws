/* global describe, it */
'use strict';

const NSHM2002Factory = require('../../src/lib/basis/nshm_2002-factory'),
    expect = require('chai').expect;

describe('nshm_2002-factory', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof NSHM2002Factory).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(NSHM2002Factory).to.not.throw(Error);
    });

    it('Sets the correct referenceDocument value', () => {
      const factory = NSHM2002Factory({
        referenceDocument: 'nshm-2002'
      });
      expect(factory.referenceDocument).to.equal('nshm-2002');
    });
  });
});
