/* global afterEach, beforeEach, describe, it */
'use strict';


const RiskCoefficientDataLoader = require('../../../src/lib/db/risk-coefficient/risk-coefficient-data-loader'),
    expect = require('chai').expect;


describe('RiskCoefficientDataLoader', () => {
  let loader;

  beforeEach(() => {
    loader = RiskCoefficientDataLoader({});
  });

  afterEach(() => {
    loader.destroy();
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof RiskCoefficientDataLoader).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(RiskCoefficientDataLoader).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(loader.destroy).to.not.throw(Error);
    });
  });
});