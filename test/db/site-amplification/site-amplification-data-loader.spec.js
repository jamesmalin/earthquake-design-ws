/* global afterEach, beforeEach, describe, it */
'use strict';


const SiteAmplificationDataLoader = require('../../../src/lib/db/site-amplification/site-amplification-data-loader.js'),
    AbstractDataLoader = require('../../../src/lib/db/abstract-data-loader.js'),
    //dbUtils = require('../../src/lib/db/db-utils'),
    expect = require('chai').expect,
    inquirer = require('inquirer'),
    sinon = require('sinon');


const MOCK_DB = {
  query: () => {
    return Promise.resolve({rows: [{}]});
  }
};

describe('SiteAmplificationDataLoader', () => {
  let loader,
      options;

  beforeEach(() => {
    options = {
      db: MOCK_DB,
      mode: SiteAmplificationDataLoader.MODE_MISSING,
      schemaName: 'schema',
      schemaUser: 'user'
    };
    loader = SiteAmplificationDataLoader(options);
  });

  afterEach(() => {
    loader.destroy();
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof SiteAmplificationDataLoader).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(SiteAmplificationDataLoader).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(loader.destroy).to.not.throw(Error);
    });
  });

  describe('insertGroundMotionLevel', () => {
    const groundMotionLevel = [0.25,0.5,0.75,1,1.25];
    const lookupIds =
    {
      '1': {
        bins: groundMotionLevel
      }
    };
    beforeEach(() => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: [{'id': 1}]});
        }
      };
      sinon.spy(loader.db, 'query');
    });

    it('inserts all lookup data when in silent mode', done => {
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader.insertGroundMotionLevel(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('1');
          expect(loader.db.query.getCall(0).args[1][1]).to.be.equal(groundMotionLevel);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });

    it('gets a site amplification id to replace existing data' , done => {
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader.insertGroundMotionLevel(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('1');
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              dropLookup: true
            });
          };
        });
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      loader.insertGroundMotionLevel(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(inquirerSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('insertLookup', () => {
    beforeEach(() => {
      loader.siteAmplificationData = {
        'ASCE7-10': {
          's1': [1],
          'ss': [1]
        }
      };
    });

    it('inserts all lookup data when in silent mode', done => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: [{'id': 1}]});
        }
      };
      sinon.spy(loader.db, 'query');
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader.insertLookup()
        .then(() => {
          expect(loader.db.query.calledTwice).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('ASCE7-10');
          expect(loader.db.query.getCall(0).args[1][1]).to.be.equal('s1');
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });

    it('gets a lookup id to replace existing data' , done => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['data already exists']});
        }
      };
      sinon.spy(loader.db, 'query');
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader.insertLookup()
        .then(() => {
          expect(loader.db.query.calledTwice).to.be.true;
          expect(loader.db.query.getCall(0).args[0].indexOf('SELECT')).to.not.be.equal(-1);
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('ASCE7-10');
          expect(loader.db.query.getCall(0).args[1][1]).to.be.equal('s1');
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              dropLookup: true
            });
          };
        });

      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['data already exists']});
        }
      };
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      sinon.spy(loader.db, 'query');
      loader.insertLookup()
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(inquirerSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('insertAmplicationFactor', () => {
    const amplificationFactor = 1;
    const lookupIds =
    {
      '1': {
        siteClasses: {
          'A': amplificationFactor,
          'B': amplificationFactor,
          'C': amplificationFactor,
          'D': amplificationFactor
        }
      }
    };
    beforeEach(() => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: [{'id': 1}]});
        }
      };
      sinon.spy(loader.db, 'query');
    });

    it('inserts all lookup data when in silent mode', done => {
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader.insertAmplicationFactor(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('1');
          expect(loader.db.query.getCall(0).args[1][1]).to.be.equal('A');
          expect(loader.db.query.getCall(0).args[1][2]).to.be.equal(amplificationFactor);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });

    it('gets a site amplification id to replace existing data' , done => {
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader.insertAmplicationFactor(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('1');
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              dropLookup: true
            });
          };
        });
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      loader.insertAmplicationFactor(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(inquirerSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('insertRestriction', () => {
    const restriction = {
      limit: 1,
      message: 'message'
    };
    const lookupIds =
    {
      '1': {
        restriction: {
          'A': restriction,
          'B': null
        }
      }
    };
    beforeEach(() => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: [{'id': 1}]});
        }
      };
      sinon.spy(loader.db, 'query');
    });

    it('inserts restriction when in silent mode and skip null value', done => {
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader.insertRestriction(lookupIds)
        .then(() => {
          expect(loader.db.query.calledOnce).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('1');
          expect(loader.db.query.getCall(0).args[1][1]).to.be.equal('A');
          expect(loader.db.query.getCall(0).args[1][2]).to.be.equal(restriction.limit);
          expect(loader.db.query.getCall(0).args[1][3]).to.be.equal(restriction.message);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });

    it('gets a site amplification id to replace existing data' , done => {
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader.insertRestriction(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(0).args[1][0]).to.be.equal('1');
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              dropRestriction: true
            });
          };
        });
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      loader.insertRestriction(lookupIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(inquirerSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('run', () => {
    it('it runs everything', done => {
      sinon.stub(loader, '_createSchema').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, 'insertLookup').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, 'insertAmplicationFactor').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, 'insertRestriction').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, 'insertGroundMotionLevel').callsFake(() => {
        return Promise.resolve([]);
      });

      loader.run()
        .then(() => {
          expect(loader._createSchema.called).to.be.true;
          expect(loader.insertLookup.called).to.be.true;
          expect(loader.insertAmplicationFactor.called).to.be.true;
          expect(loader.insertRestriction.called).to.be.true;
          expect(loader.insertGroundMotionLevel.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
  });
});