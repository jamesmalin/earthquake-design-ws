/* global afterEach, beforeEach, describe, it */
'use strict';


const DeterministicDataLoader = require('../../../src/lib/db//deterministic/deterministic-data-loader'),
    expect = require('chai').expect;


describe('DeterministicDataLoader', () => {
  let loader;

  beforeEach(() => {
    loader = DeterministicDataLoader({});
  });

  afterEach(() => {
    loader.destroy();
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof DeterministicDataLoader).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(DeterministicDataLoader).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(loader.destroy).to.not.throw(Error);
    });
  });
});