/* global afterEach, beforeEach, describe, it */
'use strict';


const AbstractDataLoader = require('../../src/lib/db/abstract-data-loader'),
    dbUtils = require('../../src/lib/db/db-utils'),
    expect = require('chai').expect,
    inquirer = require('inquirer'),
    sinon = require('sinon');


const MOCK_DB = {
  query: () => {
    return Promise.resolve({rows: [{}]});
  }
};

describe('AbstractDataLoader', () => {
  let loader,
      options;

  beforeEach(() => {
    options = {
      db: MOCK_DB,
      mode: AbstractDataLoader.MODE_MISSING,
      schemaName: 'schema',
      schemaUser: 'user'
    };
    loader = AbstractDataLoader(options);
  });

  afterEach(() => {
    loader.destroy();
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof AbstractDataLoader).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(AbstractDataLoader).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(loader.destroy).to.not.throw(Error);
    });
  });

  describe('_createSchema', () => {
    const dbUtilsSpy = sinon.stub(dbUtils, 'createSchema').callsFake(() => {
      return;
    });

    it('fails without a schema name/user', () => {
      loader.schemaName = 'schema';
      loader.schemaUser = null;
      expect(loader._createSchema).to.throw(Error);
      loader.schemaName = null;
      loader.schemaUser = 'user';
      expect(loader._createSchema).to.throw(Error);
      loader.schemaName = 'schema';
      loader.schemaUser = 'user';
      expect(loader._createSchema).to.not.throw(Error);
    });

    it('call db-utils.createSchema when in silent mode', () => {
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader._createSchema();
      expect(dbUtilsSpy.called).to.be.true;
      expect(dbUtilsSpy.getCall(0).args[0].db).to.be.equal(options.db);
    });

    it('creates schema when it does not exist' , () => {
      loader._createSchema();
      expect(dbUtilsSpy.called).to.be.true;
      expect(dbUtilsSpy.getCall(0).args[0].db).to.be.equal(options.db);
    });

    it('skips creating schema when it already exists' , done => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
      sinon.spy(loader.db, 'query');
      loader._createSchema()
        .then(() => {
          expect(loader.db.query.calledTwice).to.be.true;
          expect(loader.db.query.getCall(0).args[1]).to.deep
            .equal([options.schemaName]);
          // check that search path is set
          const query = loader.db.query.getCall(1).args[0];
          expect(query.indexOf('SET search_path TO ' + options.schemaName))
            .to.not.be.equal(-1);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              createSchema: true
            });
          };
        });

      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      sinon.spy(loader.db, 'query');
      loader._createSchema()
        .then(() => {
          expect(inquirerSpy.called).to.be.true;
          expect(dbUtilsSpy.called).to.be.true;
          expect(dbUtilsSpy.getCall(0).args[0].db).to.be.equal(options.db);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('_getRegionInsert*', () => {
    it('returns the region insert query', () => {
      const query = loader._getRegionInsertQuery();
      expect(query.indexOf('INSERT INTO')).to.not.be.equal(-1);
    });
    it('returns the region insert params', () => {
      const params = loader._getRegionInsertParams({});
      expect(params.length).to.be.equal(8);
    });
  });

  describe('_insertRegions', () => {
    let regionParamsSpy,
        regionQuerySpy;

    beforeEach(() => {
      regionQuerySpy = sinon.stub(loader, '_getRegionInsertQuery')
        .callsFake(() => {
          return;
        });
      regionParamsSpy = sinon.stub(loader, '_getRegionInsertParams')
        .callsFake(() => {
          return;
        });
      loader.regions = [{}];
    });

    it('inserts all regions when in silent mode', done => {
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
      sinon.spy(loader.db, 'query');
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader._insertRegions()
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(regionQuerySpy.called).to.be.true;
          expect(regionParamsSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });

    it('saves region id for later loading' , done => {
      const regionName = 'test';
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
      loader.regions = [
        {name: regionName}
      ];
      sinon.spy(loader.db, 'query');
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader._insertRegions()
        .then(() => {
          expect(loader.db.query.calledOnce).to.be.true;
          expect(loader.db.query.getCall(0).args[1]).to.deep
            .equal([regionName]);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              createSchema: true
            });
          };
        });

      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      sinon.spy(loader.db, 'query');
      loader._insertRegions()
        .then(() => {
          expect(inquirerSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('_insertDocument', () => {
    beforeEach(() => {
      loader.documents =[{
        name: 'test',
        regions: [
          'ak'
        ]
      }];
      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
    });

    it('inserts all documents when in silent mode', done => {
      const regionIds = {
        'ak': 1
      };
      sinon.spy(loader.db, 'query');
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader._insertDocuments(regionIds)
        .then(() => {
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(0).args[1]).to.deep.equal(
              [regionIds['ak'], loader.documents[0].name]);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });

    it('skips loading document' , done => {
      sinon.spy(loader.db, 'query');
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader._insertDocuments()
        .then(() => {
          expect(loader.db.query.calledOnce).to.be.true;
          expect(loader.db.query.getCall(0).args[1]).to.deep.equal(
              [loader.documents[0].name]);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();

          done(err);
        });
    });

    it('prompts the user when in interactive mode' , done => {
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              createSchema: true
            });
          };
        });

      loader.db =  {
        // fake query where schema exists
        query: () => {
          return Promise.resolve({rows: ['schema already exists']});
        }
      };
      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      sinon.spy(loader.db, 'query');
      loader._insertDocuments()
        .then(() => {
          expect(inquirerSpy.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          inquirerSpy.restore();

          done(err);
        });
    });
  });

  describe('_insertData', () => {
    let shouldInsertSpy,
        griddedInserterSpy;

    beforeEach(() => {
      loader.regions = [
        {
          name: 'ak'
        }
      ];
      griddedInserterSpy = sinon.stub(loader.griddedInserter, 'insertGrid')
        .callsFake(() => {
          return;
        });
    });

    it('inserts new data', done => {
      shouldInsertSpy = sinon.stub(loader, '_shouldInsertData').callsFake(() => {
        return Promise.resolve({});
      });

      loader._insertData([])
        .then(() => {
          expect(shouldInsertSpy.calledOnce).to.be.true;
          expect(griddedInserterSpy.calledOnce).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          shouldInsertSpy.restore();
          griddedInserterSpy.restore();

          done(err);
        });
    });

    it('does not insert already loaded data', done => {
      shouldInsertSpy = sinon.stub(loader, '_shouldInsertData').callsFake(() => {
        return Promise.resolve(null);
      });

      loader._insertData([])
        .then(() => {
          expect(shouldInsertSpy.calledOnce).to.be.true;
          expect(griddedInserterSpy.calledOnce).to.be.false;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          shouldInsertSpy.restore();
          griddedInserterSpy.restore();

          done(err);
        });
    });

    it('failed to load data', done => {
      const dummyError = new Error('dummy error');
      shouldInsertSpy = sinon.stub(loader, '_shouldInsertData').throws(dummyError);

      loader._insertData([])
        .then(() => {
          expect(shouldInsertSpy.calledOnce).to.be.true;
          expect(griddedInserterSpy.calledOnce).to.be.false;
        })
        .catch(err => {
          expect(err).to.equal(dummyError);
          return err;
        })
        .then(() => {
          loader.destroy();
          shouldInsertSpy.restore();
          griddedInserterSpy.restore();
          done();
        });
    });
  });

  describe('_createIndexes', () => {
    it('it creates the indexes', done => {
      sinon.stub(dbUtils, 'readSqlFile').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(dbUtils, 'exec').callsFake(() => {
        return;
      });
      loader._createIndexes()
        .then(() => {
          expect(dbUtils.readSqlFile.called).to.be.true;
          expect(dbUtils.exec.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
  });

  describe('_hasData', done => {
    it('checks for the region in the database', () => {
      const region = {
        name: 'ak'
      };
      const regionIds = {
        'ak': {}
      };
      sinon.stub(loader.db, 'query').callsFake(() => {
        return Promise.resolve({
          rows: [{
            region_id: 'ak'
          }]
        });
      });
      loader._hasData(region, regionIds)
        .then(result => {
          expect(loader.db.query.called).to.be.true;
          expect(result).to.be.equal('ak');
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
  });

  describe('_shouldInsertData', () => {
    let region,
        regionIds;

    beforeEach(() => {
      region = {
        name: 'ak'
      };
      regionIds = {
        'ak': {}
      };
    });
    it('if mode is silent, load data', done => {
      loader.mode = AbstractDataLoader.MODE_SILENT;
      loader._shouldInsertData(region, regionIds)
        .then(result => {
          expect(result).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
    it('data does not exist, load data', done => {
      sinon.stub(loader, '_hasData').callsFake(() => {
        return Promise.resolve(false);
      });
      loader._shouldInsertData(region, regionIds)
        .then(result => {
          expect(result).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
    it('if mode missing and data exists, load nothing', done => {
      sinon.stub(loader, '_hasData').callsFake(() => {
        return Promise.resolve(false);
      });
      loader.mode = AbstractDataLoader.MODE_MISSING;
      loader._shouldInsertData(region, regionIds)
        .then(result => {
          expect(result).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
    it('if data exists, prompt user to drop data', done => {
      sinon.stub(loader, '_hasData').callsFake(() => {
        return Promise.resolve(true);
      });
      const inquirerSpy = sinon.stub(inquirer, 'createPromptModule')
        .callsFake(() => {
          return () => {
            return Promise.resolve({
              dropData: true
            });
          };
        });

      loader.mode = AbstractDataLoader.MODE_INTERACTIVE;
      loader._shouldInsertData(region, regionIds)
        .then(() => {
          expect(inquirerSpy.called).to.be.true;
          expect(loader.db.query.called).to.be.true;
          expect(loader.db.query.getCall(1).args[1]).to.deep
            .equal([regionIds[region.name]]);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          inquirerSpy.restore();
          loader.destroy();
          done(err);
        });
    });
  });

  describe('run', () => {
    it('it runs everything', done => {
      sinon.stub(loader, '_createIndexes').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, '_createSchema').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, '_insertRegions').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, '_insertDocuments').callsFake(() => {
        return Promise.resolve([]);
      });
      sinon.stub(loader, '_insertData').callsFake(() => {
        return Promise.resolve([]);
      });

      loader.run()
        .then(() => {
          expect(loader._createIndexes.called).to.be.true;
          expect(loader._createSchema.called).to.be.true;
          expect(loader._insertRegions.called).to.be.true;
          expect(loader._insertDocuments.called).to.be.true;
          expect(loader._insertData.called).to.be.true;
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          loader.destroy();
          done(err);
        });
    });
  });

});