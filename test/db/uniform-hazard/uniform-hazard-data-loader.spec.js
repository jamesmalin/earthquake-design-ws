/* global afterEach, beforeEach, describe, it */
'use strict';


const UniformHazardDataLoader = require('../../../src/lib/db/uniform-hazard/uniform-hazard-data-loader'),
    expect = require('chai').expect;


describe('UniformHazardDataLoader', () => {
  let loader;

  beforeEach(() => {
    loader = UniformHazardDataLoader({});
  });

  afterEach(() => {
    loader.destroy();
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof UniformHazardDataLoader).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(UniformHazardDataLoader).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(loader.destroy).to.not.throw(Error);
    });
  });
});