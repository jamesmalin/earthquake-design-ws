/* global describe, it */
'use strict';


const DesignHandler = require('../../src/lib/nehrp/nehrp-2020-handler'),
    expect = require('chai').expect;

const _FACTORY_RESULT = {
  basicDesign: {
    ss: null,
    s1: null,
    pga: null,
    ssuh: null,
    ssrt: null,
    ssd: null,
    s1uh: null,
    s1rt: null,
    s1d: null,
    pgad: null
  },
  deterministic: {
    pgad: null,
    sad: {
      periods: [],
      values: []
    }
  },
  finalDesign: {
    pgam: null,
    sms: null,
    sm1: null,
    sam: [1,1],
    sadm: [1,1],
    sds: null,
    sd1: null
  },
  metadata: {
    vs30: null,
    modelVersion: null,
    pgadFloor: null,
    pgadPercentileFactor: null,
    sadFloor: {
      periods: [],
      values: []
    },
    sadPercentileFactor: {
      periods: [],
      values: []
    },
    sauhMaxDirectionFactor: {
      periods: [],
      values: []
    },
    spatialInterpolationMethod: null
  },
  uniformHazard: {
    pgauh: null,
    sauh: {
      periods: [],
      values: []
    }
  },
  riskCoefficients: {
    cr: {
      periods: [],
      values: []
    }
  },
  siteAmplification: {
    fa: null,
    fa_note: null,
    fv: null,
    fv_note: null
  },
  designCategory: {
    sdcs: null,
    sdc1: null,
    sdc: null
  },
  spectra: {
    smSpectrum: [],
    sdSpectrum: []
  }
};

const _DESIGN_FACTORY = {
  get: () => {
    return Promise.resolve(_FACTORY_RESULT);
  }
};

describe('nehrp-2020-handler', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof DesignHandler).to.equal('function');
    });

    it('can be instantiated', () => {
      expect(DesignHandler).to.not.throw(Error);
    });
  });

  describe('convertSpectrum', () => {
    it('splits values into two arrrays', () => {
      const spectrum = [[1,2],[1,2]];
      const handler = DesignHandler({ factory: _DESIGN_FACTORY });
      const results = handler.convertSpectrum(spectrum);
      expect(results.periods).to.deep.equal([1,1]);
      expect(results.values).to.deep.equal([2,2]);
    });
  });


  describe('formatResult', () => {
    it('returns a promise', () => {
      let handler;

      handler = DesignHandler({ factory: _DESIGN_FACTORY });

      expect(handler.formatResult(_FACTORY_RESULT).catch(p => p)).to.be.instanceof(Promise);

      handler.destroy();
    });

    it('resolves with expected data structure', done => {
      let handler;

      handler = DesignHandler({ factory: _DESIGN_FACTORY });

      handler
        .formatResult(_FACTORY_RESULT)
        .then(formatted => {
          [
            'pgam',
            'sms',
            'sds',
            'sm1',
            'sd1',
            'sdc',
            'tl',
            'cv',
            'sadmSpectrum',
            'samSpectrum',
            'sdSpectrum',
            'smSpectrum',
            'savSpectrum',
            'samvSpectrum',
            'rawResults'
          ].forEach(key => {
            expect(formatted.data.hasOwnProperty(key)).to.equal(true);
          });
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          handler.destroy();
          done(err);
        });
    });
  });

});
