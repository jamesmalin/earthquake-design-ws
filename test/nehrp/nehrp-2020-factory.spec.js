/* global describe, it */
'use strict';

const NEHRP2020Factory = require('../../src/lib/nehrp/nehrp-2020-factory'),
    expect = require('chai').expect,
    sinon = require('sinon');

const _DUMMY_FACTORY = {
  deterministicService: {
    getData: () => {
      return Promise.resolve({ response: { data: {} } });
    }
  },
  metadataService: {
    getData: () => {
      return Promise.resolve({ response: { data: {} } });
    }
  },
  riskCoefficientService: {
    getData: () => {
      return Promise.resolve({ response: { data: {} } });
    }
  },
  tSubLService: {
    getData: () => {
      return Promise.resolve({ response: { data: {tl: 1} } });
    }
  },
  uniformHazardService: {
    getData: () => {
      return Promise.resolve({ response: { data: {} } });
    }
  }
};


describe('nehrp-2020-factory', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof NEHRP2020Factory).to.not.equal('undefined');
    });

    it('can be instantiated', () => {
      expect(NEHRP2020Factory).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      const factory = NEHRP2020Factory();
      expect(factory.destroy).to.not.throw(Error);
      factory.destroy();
    });
  });

  describe('computeFinalDesign', () => {
    it('returns a finalDesign', () => {
      let factory;

      const uniformHazard = {
        sauh: {
          periods: [0, 0.01, 0.02, 0.03, 0.05, 0.075, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.5, 2, 3, 4, 5, 7.5, 10],
          values: []
        },
        pgauh: 1
      };

      const metadata = {
        pgadFloor: 1,
        pgadPercentileFactor: 1,
        sadPercentileFactor: {
          values: []
        },
        sadFloor: {
          values: []
        },
        sauhMaxDirectionFactor: {
          values: []
        },
        vs30: 1
      };

      const deterministic = {
        sad: {
          values: []
        }
      };

      const riskCoefficient = {
        cr: {
          values: []
        }
      };

      factory = NEHRP2020Factory(_DUMMY_FACTORY);
      const finalDesign = factory.computeFinalDesign(
          uniformHazard,
          deterministic,
          riskCoefficient,
          metadata
      );
      expect(finalDesign.pgam).to.not.be.undefined;
      expect(finalDesign.sms).to.not.be.undefined;
      expect(finalDesign.sm1).to.not.be.undefined;
      expect(finalDesign.sam).to.not.be.undefined;
      expect(finalDesign.sadm).to.not.be.undefined;
      expect(finalDesign.sds).to.not.be.undefined;
      expect(finalDesign.sd1).to.not.be.undefined;
      factory.destroy();
    });
  });

  describe('get', () => {
    it('returns a promise', () => {
      var factory;

      factory = NEHRP2020Factory(_DUMMY_FACTORY);
      sinon.spy(factory.metadataService, 'getData');
      sinon.spy(factory.uniformHazardService, 'getData');
      sinon.spy(factory.deterministicService, 'getData');
      sinon.spy(factory.riskCoefficientService, 'getData');
      sinon.spy(factory.tSubLService, 'getData');
      sinon.stub(factory, 'computeFinalDesign').callsFake(() => {
        return Promise.resolve({
          sms: 1,
          sm1: 1
        });
      });
      sinon.stub(factory, 'computeSpectra').callsFake(() => {
        return Promise.resolve({});
      });

      factory.get().then(result => {
        expect(result).to.be.instanceof(Promise);
        expect(factory.metadataService.callCount).to.equal(1);
        expect(factory.uniformHazardService.callCount).to.equal(1);
        expect(factory.deterministicService.callCount).to.equal(1);
        expect(factory.riskCoefficientService.callCount).to.equal(1);
        expect(factory.computeFinalDesign.callCount).to.equal(1);
        expect(factory.computeSpectra.callCount).to.equal(1);
      });
      factory.destroy();
    });
  });
});
