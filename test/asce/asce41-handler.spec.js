/* global afterEach, beforeEach, describe, it */
'use strict';

const ASCE41Handler = require('../../src/lib/asce/asce41-handler'),
    expect = require('chai').expect,
    sinon = require('sinon');

const _RESULT = {
  data: [],
  metadata: {
    curveInterpolationMethod: undefined,
    modelVersion: undefined,
    s1MaxDirFactor: null,
    s1dFloor: null,
    s1dPercentileFactor: null,
    spatialInterpolationMethod: undefined,
    ssMaxDirFactor: null,
    ssdFloor: null,
    ssdPercentileFactor: null
  }
};

const _FACTORY = function() {
  return {
    destroy: () => {
      // Nothing to do here
    },
    get: () => {
      return Promise.resolve({ data: [], metadata: {} });
    }
  };
};

describe('asce41-handler', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof ASCE41Handler).to.equal('function');
    });

    it('can be instantiated', () => {
      // if a factory is not passed in, then an error will be thrown
      expect(() => {
        ASCE41Handler({ factory: _FACTORY });
      }).to.not.throw(Error);
      expect(ASCE41Handler).to.throw(Error);
    });

    it('can be destroyed', () => {
      const handler = ASCE41Handler({ factory: _FACTORY });
      expect(handler.destroy).to.not.throw(Error);
    });
  });

  describe('checkParams', () => {
    let handler;

    afterEach(() => {
      handler.destroy();
    });

    beforeEach(() => {
      handler = ASCE41Handler({ factory: _FACTORY });
    });

    it('returns error if parameters are missing', done => {
      handler
        .checkParams({})
        .then(() => {
          return new Error('checkParams passed erroneously');
        })
        .catch(err => {
          expect(err).to.be.an.instanceof(Error);
          expect(err.status).to.equal(400);
        })
        .then(done);
    });
  });


  describe('formatResult', () => {
    it('returns a promise', () => {
      const handler = ASCE41Handler({ factory: _FACTORY });

      expect(handler.formatResult()).to.be.instanceof(Promise);

      handler.destroy();
    });

    it('resolves with expected data structure', done => {
      const handler = ASCE41Handler({ factory: _FACTORY });
      const result = {
        data: [
          {
            hazardLevel: 'BSE-2N',
            ssuh: 2.49436,
            crs: 0.92512,
            ssrt: 2.3075823232,
            ssd: 2.7300240000000002,
            ss: 2.3075823232,
            fa: 1,
            sxs: 2.3075823232,
            s1uh: 0.8775390000000001,
            cr1: 0.93991,
            s1rt: 0.82480768149,
            s1d: 1.004094,
            s1: 0.82480768149,
            fv: 1.3,
            sx1: 1.072249985937,
            horizontalSpectrum: []
          }
        ],
        metadata: {
          vs30: 760,
          curveInterpolationMethod: 'logloglinear',
          modelVersion: 'v3.1.x',
          sadFloor: {
            periods: [],
            values: []
          },
          sadPercentileFactor: {
            periods: [],
            values: []
          },
          sauhMaxDirectionFactor: {
            periods: [],
            values: []
          },
          spatialInterpolationMethod: 'linearlinearlinear'
        }
      };

      handler
        .formatResult(result)
        .then(formatted => {
          // This might seem dumb, but the formatter is outputting everything it gets
          Object.keys(result.data).forEach(key => {
            expect(formatted.data.hasOwnProperty(key)).to.equal(true);
          });
          [
            'curveInterpolationMethod',
            'modelVersion',
            's1MaxDirFactor',
            's1dFloor',
            's1dPercentileFactor',
            'spatialInterpolationMethod',
            'ssMaxDirFactor',
            'ssdFloor',
            'ssdPercentileFactor'
          ].forEach(key => {
            expect(formatted.metadata.hasOwnProperty(key)).to.equal(true);
          });
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          handler.destroy();
          done(err);
        });
    });
  });

  describe('get', () => {
    let handler;

    afterEach(() => {
      handler.destroy();
    });

    beforeEach(() => {
      handler = ASCE41Handler({ factory: _FACTORY });
    });

    it('returns an object with data', done => {
      sinon.stub(handler, 'checkParams').callsFake(() => {
        return Promise.resolve({});
      });

      handler
        .get({})
        .then(params => {
          expect(params).to.deep.equal(_RESULT);
        })
        .catch(err => {
          return err;
        })
        .then(done);

      handler.checkParams.restore();
    });
  });
});
