/* global afterEach, beforeEach, describe, it */
'use strict';

const expect = require('chai').expect,
    MetadataFactory = require('../../src/lib/component/metadata-factory'),
    sinon = require('sinon');

const _DUMMY_INPUTS = {
  latitude: 35,
  longitude: -105,
  referenceDocument: 'ASCE7-10'
};

const _DUMMY_DB = {
  query: () => {
    return Promise.resolve({
      response: {
        data: {
          curveinterpolationmethod: 'linearlinearlinear',
          gridspacing: 0.01,
          modelversion: 'v3.1.x',
          pgadfloor: 0.6,
          pgadpercentilefactor: 1.8,
          regionname: 'E2008R2_COUS0P01_UniformHazard',
          sadfloor: [1, 1.3],
          sauhmaxdirectionfactor: [1, 1.3],
          sadpercentilefactor: [1.8, 1.8],
          spatialinterpolationmethod: 'linearlinearlinear'
        }
      }
    });
  }
};

describe('MetadataFactory', () => {
  let factory;

  beforeEach(() => {
    factory = MetadataFactory({
      db: _DUMMY_DB
    });
  });

  afterEach(() => {
    factory.destroy();
    factory = null;
  });

  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof MetadataFactory).to.equal('function');
    });

    it('can be instantiated', () => {
      expect(MetadataFactory).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      expect(() => {
        let factory;

        factory = MetadataFactory();
        factory.destroy();
        factory = null;
      }).to.not.throw(Error);
    });
  });

  describe('_computeRegionArea', () => {
    let region;

    region = {
      max_latitude: 45.0,
      max_longitude: 112.0,
      min_latitude: 40.0,
      min_longitude: 110.0
    };

    it('returns correct area', () => {
      expect(factory._computeRegionArea(region)).to.equal(10);
    });
  });

  describe('getMetadata', () => {
    it('returns a promise', () => {
      expect(factory.getMetadata(_DUMMY_INPUTS)).to.be.an.instanceof(Promise);
    });

    it('calls each factory method', done => {
      let result;

      sinon.stub(factory, 'getData').callsFake(() => {
        return Promise.resolve();
      });

      sinon.stub(factory, 'getRegion').callsFake(() => {
        return Promise.resolve();
      });

      result = factory.getMetadata({
        latitude: 35,
        longitude: -105,
        referenceDocument: 'ASCE41-13'
      });

      result
        .then(() => {
          expect(factory.getData.callCount).to.equal(1);
          expect(factory.getRegion.callCount).to.equal(1);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          try {
            factory.getData.restore();
            factory.getRegion.restore();
          } catch (e) {
            err = err || e;
          }
          done(err);
        });
    });
  });

  describe('getData', () => {
    it('returns a promise', () => {
      expect(
          factory.getData('CANV0P01', {
            id: 6,
            periods: [0.2, 1],
            vs30: 760
          })
      ).to.be.an.instanceof(Promise);
    });

    it('queries the database', done => {
      let factory, getDataResults, referenceDocument, region;

      getDataResults = {
        rows: [
          {
            periods: [0.2, 1],
            vs30: 'BC',
            curveinterpolationmethod: 'linearlinearlinear',
            modelversion: 'v4.0.x',
            pgadfloor: 0.5,
            pgadpercentilefactor: 1.8,
            sadfloor: [1.5, 0.6],
            sadpercentilefactor: [1.8, 1.8],
            sauhmaxdirectionfactor: [1.1, 1.3],
            spatialinterpolationmethod: 'linearloglinear'
          }
        ]
      };
      referenceDocument = 'ASCE7-05';
      region = { id: 6, periods: [0.2, 1], vs30: 760 };

      factory = MetadataFactory({
        db: {
          query: sinon.spy(() => {
            return Promise.resolve(getDataResults);
          })
        }
      });

      factory
        .getData(referenceDocument, region)
        .then(data => {
          // check query
          expect(factory.db.query.callCount).to.equal(1);
          expect(
              factory.db.query.calledWith(factory.queryData, [
                referenceDocument,
                region.id
              ])
          ).to.be.true;

          // metadata query results, unformatted
          expect(data).to.deep.equal({
            region: { id: 6, periods: [0.2, 1], vs30: 760 },
            rows: [
              {
                periods: [0.2, 1],
                vs30: 'BC',
                curveinterpolationmethod: 'linearlinearlinear',
                modelversion: 'v4.0.x',
                pgadfloor: 0.5,
                pgadpercentilefactor: 1.8,
                sadfloor: [1.5, 0.6],
                sadpercentilefactor: [1.8, 1.8],
                sauhmaxdirectionfactor: [1.1, 1.3],
                spatialinterpolationmethod: 'linearloglinear'
              }
            ]
          });
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          try {
            factory.destroy();
          } catch (e) {
            err = err || e;
          }
          done(err);
        });
    });
  });

  describe('siteClassToVs30', () => {
    it('converts valid string to number', () => {
      const num = factory.siteClassToVs30('BC');
      expect(num).to.equal(760);
    });

    it('converts valid lowercase string to number', () => {
      const num = factory.siteClassToVs30('de');
      expect(num).to.equal(185);
    });

    it('converts all valid strings to numbers', () => {
      const nums = [2000, 1500, 1080, 760, 530, 365, 260, 185, 150];
      const chars = ['A', 'AB', 'B', 'BC', 'C', 'CD', 'D', 'DE', 'E'];
      for (let i = 0; i < nums.length; i++) {
        const num = factory.siteClassToVs30(chars[i]);
        expect(num).to.equal(nums[i]);
      }
    });

    it('returns undefined with bad parameter', () => {
      const num = factory.siteClassToVs30('Z');
      expect(num).to.be.undefined;
    });

    it('returns null with null input', () => {
      const num = factory.siteClassToVs30(null);
      expect(num).to.be.null;
    });
  });

  describe('getRegion', () => {
    it('returns a promise', () => {
      expect(factory.getRegion()).to.be.an.instanceof(Promise);
    });

    it('queries the database', done => {
      let factory,
          getRegionResults,
          latitude,
          longitude,
          referenceDocument,
          siteClass;

      getRegionResults = {
        rows: [
          {
            id: 4,
            grid_spacing: 0.01,
            max_latitude: 42,
            max_longitude: -115,
            min_latitude: 32,
            min_longitude: -125,
            name: 'CANV0P01',
            vs30: 760
          },
          {
            id: 13,
            grid_spacing: 0.05,
            max_latitude: 50,
            max_longitude: -65,
            min_latitude: 24.6,
            min_longitude: -125,
            name: 'US0P05',
            vs30: 760
          }
        ]
      };
      referenceDocument = 'ASCE7-05';
      latitude = 35;
      longitude = -118;
      siteClass = 760;

      factory = MetadataFactory({
        db: {
          query: () => {
            return Promise.resolve();
          }
        }
      });

      sinon.stub(factory.db, 'query').callsFake(() => {
        return Promise.resolve(getRegionResults);
      });

      sinon.spy(factory, '_computeRegionArea');
      sinon.spy(factory, 'getRegion');

      factory
        .getRegion(latitude, longitude, referenceDocument, siteClass)
        .then(region => {
          // check params
          expect(
              factory.getRegion.calledWith(
                  latitude,
                  longitude,
                  referenceDocument,
                  siteClass
              )
          ).to.be.true;

          // check query
          expect(factory.db.query.callCount).to.equal(1);
          expect(
              factory.db.query.calledWith(factory.queryRegion, [
                latitude,
                longitude,
                referenceDocument,
                siteClass
              ])
          ).to.be.true;

          // check submethods are called
          expect(factory._computeRegionArea.callCount).to.equal(2);

          // check region results
          expect(region.id).to.deep.equal(getRegionResults.rows[0].id);
        })
        .catch(err => {
          return err;
        })
        .then(err => {
          try {
            factory.db.query.restore();
          } catch (e) {
            err = err || e;
          }
          done(err);
        });
    });
  });
});
