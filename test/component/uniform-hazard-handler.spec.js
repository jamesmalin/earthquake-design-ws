/* global describe, it */
'use strict';

const UniformHazardHandler = require('../../src/lib/component/uniform-hazard-handler'),
    expect = require('chai').expect;

describe('uniform-hazard-handler', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof UniformHazardHandler).to.equal('function');
    });

    it('can be instantiated', () => {
      expect(UniformHazardHandler).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      const handler = UniformHazardHandler();
      expect(handler.destroy).to.not.throw(Error);
    });
  });

  describe('formatData', () => {
    const handler = UniformHazardHandler();

    it('Returns expected data structure', () => {
      const result = handler.formatData({
        metadata: {
          region: {
            periods: [1, 2, 3, 4, 5]
          }
        },
        data: {
          pgauh: -1,
          sauh: [6, 7, 8, 9, 10]
        }
      });

      expect(result).to.deep.equal({
        pgauh: -1,
        sauh: {
          periods: [1, 2, 3, 4, 5],
          values: [6, 7, 8, 9, 10]
        }
      });
    });
  });
});
