/* global describe, it */
'use strict';

const DeterministicHandler = require('../../src/lib/component/deterministic-handler'),
    expect = require('chai').expect;

describe('deterministic-handler', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof DeterministicHandler).to.equal('function');
    });

    it('can be instantiated', () => {
      expect(DeterministicHandler).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      const handler = DeterministicHandler();
      expect(handler.destroy).to.not.throw(Error);
    });
  });

  describe('formatData', () => {
    const handler = DeterministicHandler();

    it('Returns expected data structure', () => {
      const result = handler.formatData({
        metadata: {
          region: {
            periods: [1, 2, 3, 4, 5]
          }
        },
        data: {
          pgad: 0,
          sad: [6, 7, 8, 9, 10]
        }
      });

      expect(result).to.deep.equal({
        pgad: 0,
        sad: {
          periods: [1, 2, 3, 4, 5],
          values: [6, 7, 8, 9, 10]
        }
      });
    });
  });
});
