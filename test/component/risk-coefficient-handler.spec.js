/* global describe, it */
'use strict';

const RiskCoefficientHandler = require('../../src/lib/component/risk-coefficient-handler'),
    expect = require('chai').expect;

describe('risk-coefficient-handler', () => {
  describe('constructor', () => {
    it('is defined', () => {
      expect(typeof RiskCoefficientHandler).to.equal('function');
    });

    it('can be instantiated', () => {
      expect(RiskCoefficientHandler).to.not.throw(Error);
    });

    it('can be destroyed', () => {
      const handler = RiskCoefficientHandler();
      expect(handler.destroy).to.not.throw(Error);
    });
  });

  describe('formatData', () => {
    const handler = RiskCoefficientHandler();

    it('Returns expected data structure', () => {
      const result = handler.formatData({
        metadata: {
          region: {
            periods: [1, 2, 3, 4, 5]
          }
        },
        data: {
          cr: [6, 7, 8, 9, 10]
        }
      });

      expect(result).to.deep.equal({
        cr: {
          periods: [1, 2, 3, 4, 5],
          values: [6, 7, 8, 9, 10]
        }
      });
    });
  });
});
