## Launch EKS Cluster using CloudFormation

### Prerequisites

Directory to store config
```
mkdir -p ~/.kube
```

#### Install kubectl
```
sudo curl --silent --location -o /usr/local/bin/kubectl "https://amazon-eks.s3-us-west-2.amazonaws.com/1.11.5/2018-12-06/bin/linux/amd64/kubectl"

sudo chmod +x /usr/local/bin/kubectl
```

#### Install AWS IAM Authenticator
```
go get -u -v github.com/kubernetes-sigs/aws-iam-authenticator/cmd/aws-iam-authenticator
sudo mv ~/go/bin/aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
```

#### Install JQ and envsubst
```
sudo yum -y install jq gettext
```

#### Verify the binaries are in the path and executable
```
for command in kubectl aws-iam-authenticator jq envsubst
  do
    which $command &>/dev/null && echo "$command in path" || echo "$command NOT FOUND"
  done
```

#### Create Cloud9 Workspace

Visit the following URL to create workspace:
https://us-west-2.console.aws.amazon.com/cloud9/home?region=us-west-2

Create IAM role for Workspace -> Select EC2 -> Add AdministratorAccess -> Name the role "usgs-admin"

Attach new role to EC2 workspace created above.

Go into Cloud9 workspace -> click the sprocket -> Select AWS SETTINGS -> Turn off AWS managed temporary credentials

#### Create SSH Key

```
ssh-keygen
```

#### Upload SSH Key to EC2

```
aws ec2 import-key-pair --key-name "usgsekscluster" --public-key-material file://~/.ssh/id_rsa.pub
```

### Create the EKS Service Role

```
aws iam create-role --role-name "usgs-eks" --assume-role-policy-document file://assets/eks-iam-trust-policy.json --description "EKS Service role"
```

### Attach IAM Policy to the EKS Service Role

```
aws iam list-attached-role-policies --role-name "usgs-eks"
```

```
aws iam attach-role-policy --role-name "usgs-eks" --policy-arn arn:aws:iam::aws:policy/AmazonEKSClusterPolicy

aws iam attach-role-policy --role-name "usgs-eks" --policy-arn arn:aws:iam::aws:policy/AmazonEKSServicePolicy

```

### Create the VPC

Launch EKS cluster into VPC
```
aws cloudformation create-stack --stack-name "usgs-eks-cluster" --template-url "https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/amazon-eks-vpc-sample.yaml"
```

#### Check if complete:
```
until [[ `aws cloudformation describe-stacks --stack-name "usgs-eks-cluster" --query "Stacks[0].[StackStatus]" --output text` == "CREATE_COMPLETE" ]]; do  echo "The stack is NOT in a state of CREATE_COMPLETE at `date`";   sleep 30; done && echo "The Stack is built at `date` - Please proceed"
```

### Create the EKS Cluster

#### Set variables
```
export SERVICE_ROLE=$(aws iam get-role --role-name "usgs-eks" --query Role.Arn --output text)

export SECURITY_GROUP=$(aws cloudformation describe-stacks --stack-name "usgs-eks-cluster" --query "Stacks[0].Outputs[?OutputKey=='SecurityGroups'].OutputValue" --output text)

export SUBNET_IDS=$( aws cloudformation describe-stacks --stack-name "usgs-eks-cluster" --query "Stacks[0].Outputs[?OutputKey=='SubnetIds'].OutputValue" --output text)
```

#### Create EKS Cluster
```
aws eks create-cluster --name usgs-eks-cluster --role-arn "${SERVICE_ROLE}" --resources-vpc-config subnetIds="${SUBNET_IDS}",securityGroupIds="${SECURITY_GROUP}"
```

#### Check Status
```
until [[ `aws eks describe-cluster --name "usgs-eks-cluster" --query cluster.status --output text` == "ACTIVE" ]]; do  echo "The EKS cluster is NOT in a state of ACTIVE at `date`";   sleep 30; done && echo "The EKS cluster is active as of `date` - Please proceed"
```

### Create KubeConfig File

#### Add environment variables
```
export EKS_ENDPOINT=$(aws eks describe-cluster --name usgs-eks-cluster --query cluster.[endpoint] --output=text)
export EKS_CA_DATA=$(aws eks describe-cluster --name usgs-eks-cluster --query cluster.[certificateAuthority.data] --output text)
```

#### Confirm variables
```
echo EKS_ENDPOINT=${EKS_ENDPOINT}
echo EKS_CA_DATA=${EKS_CA_DATA}
```

#### Create KubeConfig file
```
mkdir ${HOME}/.kube

cat <<EoF > ${HOME}/.kube/config-usgs-eks-cluster
  apiVersion: v1
  clusters:
  - cluster:
      server: ${EKS_ENDPOINT}
      certificate-authority-data: ${EKS_CA_DATA}
    name: kubernetes
  contexts:
  - context:
      cluster: kubernetes
      user: aws
    name: aws
  current-context: aws
  kind: Config
  preferences: {}
  users:
  - name: aws
    user:
      exec:
        apiVersion: client.authentication.k8s.io/v1alpha1
        command: aws-iam-authenticator
        args:
          - "token"
          - "-i"
          - "usgs-eks-cluster"
EoF
```

#### Add config to KubeCtl Config list:
```
export KUBECONFIG=${HOME}/.kube/config-usgs-eks-cluster
echo "export KUBECONFIG=${KUBECONFIG}" >> ${HOME}/.bashrc
```

#### Confirm KubeConfig is available
```
kubectl config view
```

#### Confirm EKS cluster can communicate with Kubernetes API:
```
kubectl get svc
```

### Add Workers

#### Use CloudFormation to launch worker nodes that will connect to the EKS cluster:
```
export SUBNET_IDS=$(aws cloudformation describe-stacks --stack-name "usgs-eks-cluster" --query "Stacks[0].Outputs[?OutputKey=='SubnetIds'].OutputValue" --output text)
export SECURITY_GROUP=$(aws cloudformation describe-stacks --stack-name "usgs-eks-cluster" --query "Stacks[0].Outputs[?OutputKey=='SecurityGroups'].OutputValue" --output text)
export VPC_ID=$(aws cloudformation describe-stacks --stack-name "usgs-eks-cluster" --query "Stacks[0].Outputs[?OutputKey=='VpcId'].OutputValue" --output text)

aws cloudformation create-stack --stack-name "usgs-eks-cluster-worker-nodes" \
--template-url "https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/amazon-eks-nodegroup.yaml" \
--capabilities CAPABILITY_IAM \
--parameters \
ParameterKey=KeyName,ParameterValue=usgsekscluster \
ParameterKey=NodeImageId,ParameterValue=ami-09b2b923ebde9c970 \
ParameterKey=NodeGroupName,ParameterValue=eks-howto-workers \
ParameterKey=ClusterControlPlaneSecurityGroup,ParameterValue=$SECURITY_GROUP \
ParameterKey=VpcId,ParameterValue=$VPC_ID \
ParameterKey=Subnets,ParameterValue=\"$SUBNET_IDS\" \
ParameterKey=ClusterName,ParameterValue=usgs-eks-cluster
```

#### Check Status
```
until [[ `aws cloudformation describe-stacks --stack-name "usgs-eks-cluster-worker-nodes" --query "Stacks[0].[StackStatus]" --output text` == "CREATE_COMPLETE" ]]; do  echo "The stack is NOT in a state of CREATE_COMPLETE at `date`";   sleep 30; done && echo "The Stack is built at `date` - Please proceed"
```

### Create the Worker ConfigMap

#### View the template
```
cd ${HOME}/documents/howto-launch-eks-workshop/

cat assets/worker-configmap.yml
```

#### Lookup and store the Instance ARN:
```
export INSTANCE_ARN=$(aws cloudformation describe-stacks --stack-name "usgs-eks-cluster-worker-nodes" --query "Stacks[0].Outputs[?OutputKey=='NodeInstanceRole'].OutputValue" --output text)

echo INSTANCE_ARN=$INSTANCE_ARN
```

#### Test modify the template to see what changes:
```
sed "s@.*rolearn.*@    - rolearn: $INSTANCE_ARN@" assets/worker-configmap.yml
```

#### Actually apply the configmap:
```
sed "s@.*rolearn.*@    - rolearn: $INSTANCE_ARN@" assets/worker-configmap.yml | kubectl apply -f /dev/stdin
```

### View the Node Status

#### View status of nodes as they become ready
```
kubectl get nodes --watch
```

### Cleanup

#### Remove the Worker nodes from EKS:
```
aws cloudformation delete-stack --stack-name "usgs-eks-cluster-worker-nodes"
```

#### Delete the EKS Cluster:
```
aws eks delete-cluster --name "usgs-eks-cluster"
```

#### Make sure EKS Cluster is DELETED:
```
aws eks describe-cluster --name usgs-eks-cluster --query "cluster.status"
```

#### Remove the Cluster VPC:
```
aws cloudformation delete-stack --stack-name "usgs-eks-cluster"
```

#### Detach IAM Policies from Role:
```
aws iam detach-role-policy --role-name "usgs-eks" --policy-arn "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
aws iam detach-role-policy --role-name "usgs-eks" --policy-arn "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
```

#### Remove the IAM Role craeted for the EKS cluster:
```
aws iam delete-role --role-name "usgs-admin"
```

#### Remove SSH Key:
```
aws ec2 delete-key-pair --key-name "usgsekscluster"
```