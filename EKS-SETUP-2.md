# EKS + CloudFormation Walkthrough

## Prerequisites
You will need to make sure you have the following components installed and set up before you start with Amazon EKS:

### AWS CLI 
– while you can use the AWS Console to create a cluster in EKS, the AWS CLI is easier. You will need version 1.16.73 at least.
### Kubectl 
– used for communicating with the cluster API server.
### AWS-IAM-Authenticator 
– to allow IAM authentication with the Kubernetes cluster.

## Create IAM role with permissions to EKS
### Log into AWS Console
Open the IAM console, select Roles on the left and then click the Create Role button at the top of the page.

From the list of AWS services, select EKS and then Next: Permissions at the bottom of the page.

Enter a name for the role (e.g. usgs-eks) and hit the Create role button at the bottom of the page to create the IAM role.

The IAM role is created.

## Create VPC with CloudFormation template
### Use from template
```
https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-01-09/amazon-eks-vpc-sample.yaml
```

### Specify Details
Name the stack. Leave default network configuration.

### Options
Leave default options.

### Review
Leave default and click create.

Once created, click on the new stack and make note of the parameters under the output tab.

## Create EKS Cluster

### Replace the region, clusterName, EKS-role-ARN, subnets and security-group-id
```
aws eks --region <region> create-cluster --name <clusterName> --role-arn <EKS-role-ARN> --resources-vpc-config subnetIds=<subnet-id-1>,<subnet-id-2>,<subnet-id-3>,securityGroupIds=<security-group-id>
```

### Example:
aws eks --region us-east-1 create-cluster --name usgs-eks-cluster --role-arn arn:aws:iam::677211076551:role/usgs-eks --resources-vpc-config subnetIds=subnet-08aab241d7a6aa81e,subnet-01abbaa3e8c2dbcb3,subnet-065199e217b01d3ff,securityGroupIds=sg-0ae5b19e67eb294f5

### Example Output:
```
{
    "cluster": {
        "name": "usgs-eks-cluster",
        "arn": "arn:aws:eks:us-east-1:677211076551:cluster/usgs-eks-cluster",
        "createdAt": 1556812463.228,
        "version": "1.12",
        "roleArn": "arn:aws:iam::677211076551:role/usgs-eks",
        "resourcesVpcConfig": {
            "subnetIds": [
                "subnet-08b9219f894b4ef7d",
                "subnet-0eb16bc67e2df109d",
                "subnet-0ee43ad41a3223259"
            ],
            "securityGroupIds": [
                "sg-06dce0b3b8221d9ce"
            ],
            "vpcId": "vpc-0cb6d33c77eac44c6"
        },
        "status": "CREATING",
        "certificateAuthority": {},
        "platformVersion": "eks.1"
    }
}
```

### Que status
Replace the name variable below. Should read CREATING at first. Once it responds with ACTIVE, you're ready to go.
```
until [[ `aws eks --region us-east-1 describe-cluster --name usgs-eks-cluster --query cluster.status --output text` == "ACTIVE" ]]; do  echo "The EKS cluster is NOT in a state of ACTIVE at `date`"; sleep 30; done && echo "The EKS cluster is active as of `date` - Please proceed"
```

### Update KubeConfig
Replace the name variable below.
```
aws eks --region us-east-1 update-kubeconfig --name usgs-eks-cluster
```

### Test configurations
```
kubectl get svc
```

## Launching Kubernetes worker nodes

### Create CloudFormation Stack
Open CloudFormation, click Create Stack, and this time use the following template URL:
```
https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/amazon-eks-nodegroup.yaml
```

### Details
Fill in the following and leave other details as is:
ClusterName – the name of your Kubernetes cluster (e.g. usgs-eks-cluster)
ClusterControlPlaneSecurityGroup – the same security group you used for creating the cluster in previous step.
NodeGroupName – a name for your node group.
NodeImageId – the Amazon EKS worker node AMI ID for the region you’re using. For us-east-1, for example: ami-0abcb9f9190e867ab
KeyName – the name of an Amazon EC2 SSH key pair for connecting with the worker nodes once they launch.
VpcId – enter the ID of the VPC you created above.
Subnets – select the three subnets you created above.

### Options
Leave as is.

### Review
Accept and click create.

### After complete, make note of Outputs

### Download the AWS authenticator configuration map
```
curl -O https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-01-09/aws-auth-cm.yaml
```

### Open the aws-auth-cm.yaml
Replace <ARN of instance role>

### Save and apply config
```
kubectl apply -f aws-auth-cm.yaml
```

### Check status of worker nodes
```
kubectl get nodes --watch
```

hint: If you don't see anything displayed for node workers, it's probably because you didn't use the correct AMI. https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html

## Demo

### Start services
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-master-controller.json
kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-master-service.json
kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-slave-controller.json
kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-slave-service.json
kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/guestbook-controller.json
kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/guestbook-service.json 
```

### To see list of services
```
kubectl get svc
```