'use strict';

// This file contains prompts the user is presented with when configuring this
// application. This should export an array of {Question} objects as defined
// by npm/inquirer. See: https://www.npmjs.com/package/inquirer#question

module.exports = [
  {
    type: 'input',
    name: 'MOUNT_PATH',
    message: 'Application mount path',
    default: '/ws/designmaps'
  },
  {
    type: 'input',
    name: 'PORT',
    message: 'Application port',
    default: '8000'
  },
  {
    type: 'input',
    name: 'LEGACY_URL',
    message: 'Legacy web service endpoint',
    default: 'https://earthquake.usgs.gov/designmaps/beta/us/service'
  },
  {
    type: 'input',
    name: 'DB_HOST',
    message: 'Database hostname',
    default: 'localhost'
  },
  {
    type: 'input',
    name: 'DB_PORT',
    message: 'Database port number',
    default: '5432'
  },
  {
    type: 'input',
    name: 'DB_DATABASE',
    message: 'Database name',
    default: 'usdesign'
  },
  {
    type: 'input',
    name: 'DB_USER',
    message: 'Database read-only user name'
  },
  {
    type: 'password',
    name: 'DB_PASSWORD',
    message: 'Database password'
  },
  {
    type: 'input',
    name: 'DB_SCHEMA_SITE_AMPLIFICATION',
    message: 'Database schema for site amplification data',
    default: 'site_amplification'
  },
  {
    type: 'input',
    name: 'DB_SCHEMA_METADATA',
    message: 'Database schema for metadata',
    default: 'metadata'
  },
  {
    type: 'input',
    name: 'DB_SCHEMA_DETERMINISTIC',
    message: 'Database schema for deterministic data',
    default: 'deterministic'
  },
  {
    type: 'input',
    name: 'DB_SCHEMA_RISK_COEFFICIENT',
    message: 'Database schema for risk coefficient data',
    default: 'risk_coefficient'
  },
  {
    type: 'input',
    name: 'DB_SCHEMA_UNIFORM_HAZARD',
    message: 'Database schema for uniform hazard data',
    default: 'uniform_hazard'
  },
  {
    type: 'input',
    name: 'DB_SCHEMA_TSUBL',
    message: 'Database schema for TL data',
    default: 'tsubl'
  },
  {
    type: 'input',
    name: 'METADATA_SERVICE_URL',
    message: 'Web service for fetching metadata',
    default: 'https://earthquake.usgs.gov/ws/designmaps/metadata.json'
  },
  {
    type: 'input',
    name: 'UNIFORM_HAZARD_SERVICE_URL',
    message:
      'Web service for fetching mapped uniform hazard ground motion data',
    default: 'https://earthquake.usgs.gov/ws/designmaps/uniform-hazard.json'
  },
  {
    type: 'input',
    name: 'RISK_COEFFICIENT_SERVICE_URL',
    message: 'Web service for fetching mapped risk coefficent data',
    default: 'https://earthquake.usgs.gov/ws/designmaps/risk-coefficient.json'
  },
  {
    type: 'input',
    name: 'DETERMINISTIC_SERVICE_URL',
    message: 'Web service for fetching mapped deterministic ground motion data',
    default: 'https://earthquake.usgs.gov/ws/designmaps/deterministic.json'
  },
  {
    type: 'input',
    name: 'TL_SERVICE_URL',
    message: 'Web service for fetching TL data',
    default: 'https://earthquake.usgs.gov/ws/designmaps/tl.json'
  },
  {
    type: 'input',
    name: 'SITE_AMPLIFICATION_SERVICE_URL',
    message: 'Web service for fetching site amplification factors',
    default: 'https://earthquake.usgs.gov/ws/designmaps/site-amplification.json'
  }
];
