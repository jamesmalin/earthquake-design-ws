<!doctype html>
<html lang="en">
<head>
  <title>Risk Coefficient Endpoint Documentation</title>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

  <link rel="stylesheet" href="https://earthquake.usgs.gov/theme/site/earthquake/index.css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons%7cMerriweather:400,400italic,700%7cSource+Sans+Pro:400,300,700"/>
  <link rel="stylesheet" href="css/index.css"/>
</head>
<body>
<main>
  <header>
    <h1>Risk Coefficient Endpoint Documentation</h1>
    <p>
      This endpoint returns ratios of risk-targeted divided by
      uniform-hazard ground motions, known as risk coefficients. The
      risk-targeted ground motions are for a target probability of
      structural collapse corresponding to 1&percnt; in 50 years.
      The uniform-hazard ground motions correspond to a probability
      of ground motion exceedance of 2&percnt; in 50 years.
    </p>
  </header>

  <div class="alert warning">
    This software is preliminary or provisional and is subject to revision.
    It is being provided to meet the need for timely best science. The
    software has not received final approval by the U.S. Geological Survey
    (USGS). No warranty, expressed or implied, is made by the USGS or the
    U.S. Government as to the functionality of the software and related
    material nor shall the fact of release constitute any such warranty.
    The software is provided on the condition that neither the USGS nor the
    U.S. Government shall be held liable for any damages resulting from the
    authorized or unauthorized use of the software.
  </div>

  <h2 id="parameters">Request Parameters</h2>
  <p>Required parameters</p>
  <dl>
    <dt id="latitude">latitude</dt>
    <dd>
      Latitude for location at which to fetch results
      <br/>Example: <code>34</code>
    </dd>

    <dt id="longitude">longitude</dt>
    <dd>
      Longitude for location at which to fetch results
      <br/>Example: <code>-118</code>
    </dd>

    <dt id="referenceDocument">referenceDocument</dt>
    <dd>
      Design code reference document corresponding to hazard dataset for
      which to fetch results
      <br/>Example: <code>ASCE41-13</code>
    </dd>

    <dt id="siteClass">siteClass</dt>
    <dd>
      <em>[Optional]</em>
      NEHRP site class identifying the site class of the risk coefficient data
      to retrieve
      <br/>Example: <code>BC</code>
    </dd>
  </dl>

  <h2 id="output">Output</h2>
  <p>Web service response</p>
  <dl>
    <dt>request</dt>
    <dd>
      <dl>
        <dt id="output-date">date</dt>
        <dd>
          Timestamp
          <br/>Example: <code>&ldquo;YYYY-MM-DDThh:mm:ssZ&rdquo;</code>
        </dd>

        <dt id="output-reference-document">referenceDocument</dt>
        <dd>
          Design code reference document corresponding to hazard dataset for
          returned results
          <br/>Example: <code>&ldquo;ASCE41-13&rdquo;</code>
        </dd>

        <dt id="output-status">status</dt>
        <dd>
          String (&ldquo;success&rdquo; or &ldquo;error&rdquo;)
          <br/>Example: <code>&ldquo;success&rdquo;</code>
        </dd>

        <dt id="output-url">url</dt>
        <dd>
          URL to reproduce this result
          <br/>Example: <code>&ldquo;<span class="url-stub">&hellip;</span>/risk-coefficient.json?latitude=34&amp;longitude=-118&amp;referenceDocument=ASCE41-13&rdquo;</code>
        </dd>
        <dt id="request-parameters">parameters</dt>
        <dd>
          <dl>
            <dt id="request-parameters-latitude">latitude</dt>
            <dd>
              Latitude for location at which to fetch results
              <br/>Example: <code>34</code>
            </dd>

            <dt id="request-parameters-longitude">longitude</dt>
            <dd>
              Longitude for location at which to fetch results
              <br/>Example: <code>-118</code>
            </dd>

            <dt id="request-parameters-siteClass">siteClass</dt>
            <dd>
              NEHRP site class for which to fetch results.
              <br/>Example: <code>&ldquo;BC&rdquo;</code>
            </dd>
          </dl>
        </dd>
      </dl>
    </dd>

    <dt id="response">response</dt>
    <dd>
      <dl>
        <dt id="response-data">data</dt>
        <dd>
          <dl>
            <dt id="response-data-cr">cr</dt>
            <dd>
              <dl>
                <dt id="response-data-cr-periods">periods</dt>
                <dd>
                  Array of spectral periods corresponding to the coeffient of
                  risk <a href="#response-data-cr-values">values</a>
                </dd>
                <dt id="response-data-cr-values">values</dt>
                <dd>
                  Array of coefficient of risk values corresponding to the
                  spectral <a href="#response-data-cr-periods">periods</a>
                </dd>
              </dl>
            </dd>
          </dl>
        </dd>

        <dt id="response-metadata">metadata</dt>
        <dd>
          <dl>
            <dt id="response-metadata-grid_spacing">gridSpacing</dt>
            <dd>
              The grid spacing (decimal degrees) for the underlying dataset
            </dd>

            <dt id="response-metadata-model_version">modelVersion</dt>
            <dd>
              Identifier for the underlying model version in the database
            </dd>

            <dt id="response-metadata-region_name">regionName</dt>
            <dd>
              Identifier for the underlying region name in the database
            </dd>

            <dt id="response-metadata-spatial_interpolation_method">
              spatialInterpolationMethod
            </dt>
            <dd>
              Identifier for the interpolation method used to obtain data for
              location of interest from underlying gridded datasets
            </dd>

            <dt id="response-metadata-vs30">vs30</dt>
            <dd>
              The time-averaged shear-wave velocity to 30 m depth for which
              the returned coefficient of risk values apply
            </dd>
          </dl>
        </dd>
      </dl>
    </dd>
  </dl>

  <h3 id="example_response">Example</h3>

  <h4>Request</h4>
  <p>
    <a href="risk-coefficient.json?latitude=34&amp;longitude=-118&amp;referenceDocument=ASCE41-13&amp;siteClass=BC"><span class="url-stub">&hellip;</span>/risk-coefficient.json?latitude=34&amp;longitude=-118&amp;referenceDocument=ASCE41-13&amp;siteClass=BC</a>
  </p>

  <h4>Response</h4>
  <div class="code">
    <pre>{
  "request": {
    "date": "2017-09-20T20:50:24.327Z",
    "referenceDocument": "ASCE41-13",
    "status": "success",
    "url": "<span class="url-stub">&hellip;</span>/risk-coefficient.json?latitude=34&longitude=-118&referenceDocument=ASCE41-13&siteClass=BC",
    "parameters": {
      "latitude": 34,
      "longitude": -118,
      "siteClass": "BC"
    }
  },
  "response": {
    "data": {
      "cr": {
        periods: [0.2, 1.0],
        values: [0.92512, 0.93991]
      }
    },
    "metadata": {
      "gridSpacing": 0.01,
      "modelVersion": "v3.1.x",
      "regionName": "E2008R2_COUS0P01_Risk_Coefficient",
      "spatialInterpolationMethod": "linearlinearlinear",
      "vs30": 760
    }
  }
}</pre>
  </div>
</main>
<footer class="site-commonnav">
  <a href="index.html">Seismic Design Web Service Documentation</a>
</footer>
<script src="js/config.js"></script>
</body>
</html>
