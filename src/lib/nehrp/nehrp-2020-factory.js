'use strict';

const NSHM2014Factory = require('../basis/nshm_2014-factory'),
    extend = require('extend'),
    NumberUtils = require('../util/number-utils').instance;

const _DEFAULTS = {
  referenceDocument: 'NEHRP-2020'
};

/**
 * Class: NEHRP2020Factory
 *
 * @param options Object
 *      Configuration options for this instance
 */
const NEHRP2020Factory = function(options) {
  let _this;

  options = extend({}, _DEFAULTS, options);
  _this = NSHM2014Factory(options);

  /**
   * Compute final design values
   */
  _this.computeFinalDesign = function(uniformHazard, deterministic, riskCoefficient, metadata) {

    const periods = uniformHazard.sauh.periods;
    const vs30 = metadata.vs30;

    let samp =
      NumberUtils.arrayMultiply(
          NumberUtils.arrayMultiply(
              uniformHazard.sauh.values,
              metadata.sauhMaxDirectionFactor.values
          ),
          riskCoefficient.cr.values
      );

    let samd =
      NumberUtils.arrayMax(
          NumberUtils.arrayMultiply(
              deterministic.sad.values,
              metadata.sadPercentileFactor.values
          ),
          metadata.sadFloor.values
      );

    const sam = NumberUtils.arrayMin(samp, samd);
    let a = periods.indexOf(0.2);
    let b = periods.indexOf(5);
    const sms = Math.max.apply(null, sam.slice(a, b)) * 0.9;

    if (vs30 > 365.76) {
      a = periods.indexOf(1);
      b = periods.indexOf(2);
    } else {
      a = periods.indexOf(1);
      b = periods.indexOf(5);
    }

    const sm1 = Math.max.apply(null,
        NumberUtils.arrayMultiply(periods.slice(a,b), sam.slice(a,b))
    );
    const sadm = sam.map(value => 2/3 * value);
    const pgam = Math.max(uniformHazard.pgauh * metadata.pgadPercentileFactor,
        metadata.pgadFloor);
    const sd1 = 2/3 * sm1;
    const sds = 2/3 * sms;

    return {
      pgam,
      sms,
      sm1,
      sam,
      sadm,
      sds,
      sd1
    };
  };

  _this.get = function(inputs) {
    const result = {};

    return Promise.all([
      _this.metadataService.getData(inputs),
      _this.uniformHazardService.getData(inputs),
      _this.deterministicService.getData(inputs),
      _this.riskCoefficientService.getData(inputs),
      _this.tSubLService.getData(inputs)
    ])
      .then(promiseResults => {
        result.metadata = promiseResults[0].response.data;
        result.uniformHazard = promiseResults[1].response.data;
        result.deterministic = promiseResults[2].response.data;
        result.riskCoefficient = promiseResults[3].response.data;
        result.tSubL = promiseResults[4].response.data.tl;

        return _this.computeFinalDesign(result.uniformHazard, result.deterministic,
            result.riskCoefficient, result.metadata);
      })
      .then(finalDesign => {
        result.finalDesign = finalDesign;

        return _this.computeSpectra(extend({}, finalDesign,
            {
              tSubL: result.tSubL,
              sds: 2/3 * finalDesign.sms,
              sd1: 2/3 * finalDesign.sm1
            }
        ));
      })
      .then(spectra => {
        result.spectra = spectra;

        return result;
      });
  };


  options = null;
  return _this;
};


module.exports = NEHRP2020Factory;
