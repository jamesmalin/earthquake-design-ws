'use strict';

const NEHRP2020Factory = require('./nehrp-2020-factory'),
    NSHMHandler = require('../basis/nshm-handler'),
    extend = require('extend'),
    NumberUtils = require('../util/number-utils').instance;

const _DEFAULTS = {
  factoryConstructor: NEHRP2020Factory,
  referenceDocument: 'NEHRP-2020'
};

/**
 * Handler for NEHRP2020Handler web service validates parameters and calls
 * factory with params.
 *
 * @param options {Object}
 *    Configuration options
 */
const NEHRP2020Handler = function(options) {
  let _this;

  options = extend({}, _DEFAULTS, options);
  _this = NSHMHandler(options);

  _this.convertSpectrum = function(spectrum) {
    const result = {
      periods: [],
      values: []
    };

    spectrum.forEach((item) => {
      result.periods.push(NumberUtils.round(item[0], _this.outputDecimals));
      result.values.push(NumberUtils.round(item[1], _this.outputDecimals));
    });

    return result;
  };

  _this.formatResult = function(result) {

    const finalDesign = result.finalDesign,
        metadata = result.metadata,
        periods = result.uniformHazard.sauh.periods,
        spectra = result.spectra;

    // TODO :: Here is the desired data structure. Filling in the values
    // is TBD.
    return Promise.resolve({
      data: {
        pgam: NumberUtils.round(finalDesign.pgam, _this.outputDecimals),
        sms: NumberUtils.round(finalDesign.sms, _this.outputDecimals),
        sds: NumberUtils.round(finalDesign.sds, _this.outputDecimals),
        sm1: NumberUtils.round(finalDesign.sm1, _this.outputDecimals),
        sd1: NumberUtils.round(finalDesign.sd1, _this.outputDecimals),
        sdc: null,
        tl: result.tSubL,
        cv: null,
        sadmSpectrum: {
          periods: periods,
          values: finalDesign.sadm.map(value => NumberUtils.round(value))
        },
        samSpectrum: {
          periods: periods,
          values: finalDesign.sam.map(value => NumberUtils.round(value))
        },
        sdSpectrum: _this.convertSpectrum(spectra.sdSpectrum),
        smSpectrum: _this.convertSpectrum(spectra.smSpectrum),
        savSpectrum: {
          periods: [],
          values: []
        },
        samvSpectrum: {
          periods: [],
          values: []
        },
        rawResults: {
          pgauh: null,
          pgad: null,
          ssuh: null,
          crs: null,
          ssrt: null,
          ssd: null,
          sdcs: null,
          s1uh: null,
          cr1: null,
          s1rt: null,
          s1d: null,
          sdc1: null
        }
      },
      metadata: metadata
    });
  };

  options = null;
  return _this;
};

module.exports = NEHRP2020Handler;
