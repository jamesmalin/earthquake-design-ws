'use strict';

var extend = require('extend'),
    GriddedDataHandler = require('./gridded-data-handler');

var _DEFAULTS;

_DEFAULTS = {
  DB_SCHEMA: 'deterministic'
};

var DeterministicHandler = function(options) {
  var _this;

  options = extend(true, {}, _DEFAULTS, options);
  if (options.hasOwnProperty('DB_SCHEMA_DETERMINISTIC')) {
    options.DB_SCHEMA = options.DB_SCHEMA_DETERMINISTIC;
  }
  _this = GriddedDataHandler(options);

  // Override any API or Helper methods from GriddedDataHander as needed.
  // ...
  /**
   * @APIMethod
   * Formats the data portion of the response. Converts 'cr' output field from
   * an array of values to an object with periods and corresponding values.
   *
   * @param result {Object}
   *      The raw results received from the factory.
   *
   * @return {Object}
   *      An object with a periods array containing the spectral periods (s) at
   *      which the corresponding values array (cr) apply.
   */
  _this.formatData = function(result) {
    const data = {
      pgad: result.data.pgad,
      sad: {
        periods: result.metadata.region.periods,
        values: result.data.sad
      }
    };

    return data;
  };

  options = null;
  return _this;
};

module.exports = DeterministicHandler;
