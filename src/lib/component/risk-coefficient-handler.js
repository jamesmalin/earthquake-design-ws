'use strict';

var extend = require('extend'),
    GriddedDataHandler = require('./gridded-data-handler');

var _DEFAULTS;

_DEFAULTS = {
  DB_SCHEMA: 'risk_coefficient'
};

var RiskCoefficientHandler = function(options) {
  var _this;

  options = extend(true, {}, _DEFAULTS, options);
  if (options.hasOwnProperty('DB_SCHEMA_RISK_COEFFICIENT')) {
    options.DB_SCHEMA = options.DB_SCHEMA_RISK_COEFFICIENT;
  }
  _this = GriddedDataHandler(options);

  // Override any API or Helper methods from GriddedDataHander as needed.
  // ...
  /**
   * @APIMethod
   * Formats the data portion of the response. Converts 'cr' output field from
   * an array of values to an object with periods and corresponding values.
   *
   * @param result {Object}
   *      The raw results received from the factory.
   *
   * @return {Object}
   *      An object with a periods array containing the spectral periods (s) at
   *      which the corresponding values array (cr) apply.
   */
  _this.formatData = function(result) {
    const data = {
      cr: {
        periods: result.metadata.region.periods,
        values: result.data.cr
      }
    };

    return data;
  };

  options = null;
  return _this;
};

module.exports = RiskCoefficientHandler;
