'use strict';


const Config = require('../util/config'),
    MetadataFactory = require('./metadata-factory'),
    extend = require('extend'),
    Pool = require('../db/pool');

const _CONFIG = Config().get();

const _DEFAULTS = {
  DB_DATABASE: 'postgres',
  DB_HOST: 'localhost',
  DB_PASSWORD: null,
  DB_PORT: 5432,
  DB_SCHEMA: _CONFIG.DB_SCHEMA_METADATA,
  DB_USER: null
};


const MetadataHandler = function (options) {
  let _this,
      _initialize;


  _this = {};

  _initialize = function (options) {
    options = extend(true, {}, _DEFAULTS, options);

    if (options.factory) {
      _this.factory = options.factory;
    } else {
      _this.destroyFactory = true;
      _this.factory = MetadataFactory({
        db: _this.createDbPool(options)
      });
    }
  };

  _this.checkParams = function (params) {
    const vs30Letters = [
      'A',
      'AB',
      'B',
      'BC',
      'C',
      'CD',
      'D',
      'DE',
      'E'
    ];
    let buf,
        err,
        latitude,
        longitude,
        referenceDocument;

    buf = [];

    latitude = params.latitude;
    longitude = params.longitude;
    referenceDocument = params.referenceDocument;

    if (typeof referenceDocument === 'undefined' ||
        referenceDocument === null) {
      buf.push('Missing required parameter: referenceDocument');
    }

    if (typeof latitude === 'undefined' || latitude === null) {
      buf.push('Missing required parameter: latitude');
    }

    if (typeof longitude === 'undefined' || longitude === null) {
      buf.push('Missing required parameter: longitude');
    }

    if (params.siteClass) {
      // parameter name in url, check for valid value
      if (
        vs30Letters.indexOf(params.siteClass.toUpperCase()) === -1
      ) {
        buf.push('Bad value for parameter: siteClass');
      }
    } else {
      // parameter name not in url, set it to default BC
      params.siteClass = 'BC';
    }
    
    if (buf.length > 0) {
      err = new Error(buf.join('\n'));
      err.status = 400;
      return Promise.reject(err);
    }

    return Promise.resolve(params);
  };

  _this.createDbPool = function (options) {
    options = options || _DEFAULTS;

    if (!_this.db) {
      _this.destroyDb = true;
      _this.db = Pool(options);
    }

    return _this.db;
  };

  _this.destroy = function () {
    if (_this === null) {
      return;
    }

    if (_this.destroyFactory && _this.factory) {
      _this.factory.destroy();
      _this.factory = null;
    }

    if (_this.destroyDb) {
      _this.db.destroy(); // Technically async, but what would we do anyway?
    }

    _initialize = null;
    _this = null;
  };

  _this.formatResult = function (result) {
    let region = result.region, resultRows = result.rows[0];
    let metadata = {}, formattedResult = {}, periods = [];

    if (region) {
      if (region.vs30) {
        metadata.vs30 = region.vs30;
      }
      if (region.periods) {
        periods = region.periods;
      }
    }

    const metadataKeyNames = {
      curveinterpolationmethod: 'curveInterpolationMethod',
      gridspacing: 'gridSpacing',
      modelversion: 'modelVersion',
      pgadfloor: 'pgadFloor',
      pgadpercentilefactor: 'pgadPercentileFactor',
      regionname: 'regionName',
      sadfloor: 'sadFloor',
      sauhmaxdirectionfactor: 'sauhMaxDirectionFactor',
      sadpercentilefactor: 'sadPercentileFactor',
      spatialinterpolationmethod: 'spatialInterpolationMethod',
    };

    Object.keys(resultRows).forEach(key => {
      if (metadataKeyNames.hasOwnProperty(key) && resultRows[key] != null) {
        metadata[metadataKeyNames[key]] = resultRows[key];
      }
    });

    return new Promise((resolve) => {

      if (metadata.sadFloor) {
        formattedResult.sadFloor = {
          periods: periods,
          values: metadata.sadFloor
        };
      }

      if (metadata.sadPercentileFactor) {
        formattedResult.sadPercentileFactor = {
          periods: periods,
          values: metadata.sadPercentileFactor
        };
      }

      if (metadata.sauhMaxDirectionFactor) {
        formattedResult.sauhMaxDirectionFactor = {
          periods: periods,
          values: metadata.sauhMaxDirectionFactor
        };
      }

      formattedResult = extend(true, {}, metadata, formattedResult);

      return resolve({
        data: formattedResult,
      });
    });
  };

  _this.get = function (params) {
    return _this.checkParams(params).then((params) => {
      return _this.factory.getMetadata(params);
    }).then((result) => {
      return _this.formatResult(result);
    });
  };


  _initialize(options);
  options = null;
  return _this;
};

module.exports = MetadataHandler;
