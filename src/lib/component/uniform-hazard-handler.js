'use strict';

var extend = require('extend'),
    GriddedDataHandler = require('./gridded-data-handler');

var _DEFAULTS;

_DEFAULTS = {
  DB_SCHEMA: 'uniform_hazard'
};

var UniformHazardHandler = function(options) {
  var _this;

  options = extend(true, {}, _DEFAULTS, options);
  if (options.hasOwnProperty('DB_SCHEMA_UNIFORM_HAZARD')) {
    options.DB_SCHEMA = options.DB_SCHEMA_UNIFORM_HAZARD;
  }
  _this = GriddedDataHandler(options);

  // Override any API or Helper methods from GriddedDataHander as needed.
  // ...
  _this.formatData = function(result) {
    const data = {
      pgauh: result.data.pgauh,
      sauh: {
        periods: result.metadata.region.periods,
        values: result.data.sauh
      }
    };

    return data;
  };

  options = null;
  return _this;
};

module.exports = UniformHazardHandler;
