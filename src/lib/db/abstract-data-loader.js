'use strict';

const dbUtils = require('./db-utils'),
    GriddedInserter = require('./gridded-inserter'),
    inquirer = require('inquirer');

const MODE_INTERACTIVE = 'interactive';
const MODE_MISSING = 'missing';
const MODE_SILENT = 'silent';

const AbstractDataLoader = function(options) {
  let _this, _initialize;

  _this = {};

  _initialize = function(options) {
    options = options || {};

    _this.options = options;
    _this.dataColumns = options.dataColumns;
    _this.db = options.db;
    _this.formats = options.formats;
    _this.documents = options.documents;
    _this.indexFile = options.indexFile;
    _this.mode = options.mode;
    _this.regions = options.regions;
    _this.schemaFile = options.schemaFile;
    _this.schemaName = options.schemaName;
    _this.schemaUser = options.schemaUser;
    _this.dataLoadOpts = options.dataLoadOpts;

    _this.griddedInserter = options.griddedInserter || GriddedInserter(options);
  };

  /**
   * Create database schema.
   *
   * Based on options.schemaFile, options.schemaName, and options.schemaUser.
   *
   * @return {Promise}
   *     promise representing schema has been created.
   */
  _this._createSchema = function() {
    let createSchema;

    if (!_this.schemaName || !_this.schemaUser) {
      throw new Error('schema name not configured');
    }

    createSchema = function() {
      return dbUtils.createSchema({
        db: _this.db,
        file: _this.schemaFile,
        name: _this.schemaName,
        user: _this.schemaUser
      });
    };

    if (_this.mode === MODE_SILENT) {
      // Recreate schema
      return createSchema();
    }

    return _this.db
      .query(
          `
      SELECT schema_name
      FROM information_schema.schemata
      WHERE schema_name = $1
    `,
          [_this.schemaName]
      )
      .then(result => {
        if (result.rows.length === 0) {
          return createSchema();
        } else {
          let skipCreateSchema = function() {
            return _this.db.query(
                'SET search_path TO ' + _this.schemaName + ', public'
            );
          };

          if (_this.mode === MODE_MISSING) {
            return skipCreateSchema();
          } else {
            let prompt = inquirer.createPromptModule();
            return prompt([
              {
                name: 'createSchema',
                type: 'confirm',
                message: `Schema ${
                  _this.schemaName
                } already exists, drop and reload schema`,
                default: false
              }
            ]).then(answers => {
              if (answers.createSchema) {
                return createSchema();
              } else {
                return skipCreateSchema();
              }
            });
          }
        }
      });
  };

  _this._getRegionInsertQuery = function() {
    return `
      INSERT INTO region (
        name,
        grid_spacing,
        max_latitude,
        max_longitude,
        min_latitude,
        min_longitude,
        periods,
        vs30
      ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
      RETURNING id
    `;
  };

  _this._getRegionInsertParams = function(region) {
    return [
      region.name,
      region.grid_spacing,
      region.max_latitude,
      region.max_longitude,
      region.min_latitude,
      region.min_longitude,
      region.periods,
      region.vs30
    ];
  };

  /**
   * Insert region metadata.
   *
   * Using options.regions.
   *
   * @return {Promise<Array<String, Int>>}
   *     resolves to mapping from region name to region id.
   */
  _this._insertRegions = function() {
    let promise, regions, regionIds;

    // TODO: get existing regions and filter if only loading missing
    // but may need to pass all regionIds for insertDocuments
    regions = _this.regions;

    // load regions
    promise = Promise.resolve();

    regionIds = {};
    regions.forEach(region => {
      promise = promise.then(() => {
        let insertRegion;

        insertRegion = function() {
          return _this.db
            .query(
                _this._getRegionInsertQuery(),
                _this._getRegionInsertParams(region)
            )
            .then(result => {
              // save region id for later data loading
              regionIds[region.name] = result.rows[0].id;
            });
        };

        if (_this.mode === MODE_SILENT) {
          return insertRegion();
        }

        return _this.db
          .query('SELECT id FROM region WHERE name=$1', [region.name])
          .then(result => {
            if (result.rows.length == 0) {
              // region not found
              return insertRegion();
            }

            // found existing region
            let regionId, skipInsertRegion;

            regionId = result.rows[0].id;
            skipInsertRegion = function() {
              // save region id for later data loading
              regionIds[region.name] = regionId;
            };

            if (_this.mode === MODE_MISSING) {
              // region already exists
              return skipInsertRegion();
            } else {
              // ask user whether to remove existing data
              let prompt = inquirer.createPromptModule();
              return prompt([
                {
                  name: 'dropRegion',
                  type: 'confirm',
                  message: `Region ${
                    region.name
                  } already exists, drop and reload region`,
                  default: false
                }
              ]).then(answers => {
                if (answers.dropRegion) {
                  return _this.db
                    .query('DELETE FROM region WHERE id=$1', [regionId])
                    .then(() => {
                      return insertRegion();
                    });
                } else {
                  return skipInsertRegion();
                }
              });
            }
          });
      });
    });

    return promise.then(() => {
      // all regions inserted, and IDs should be set
      return regionIds;
    });
  };

  /**
   * Insert document metadata.
   *
   * Using options.documents.
   *
   * @return {Promise}
   *     promise representing document metadata being inserted.
   */
  _this._insertDocuments = function(regionIds) {
    let promise;

    promise = Promise.resolve();

    _this.documents.forEach(doc => {
      promise = promise.then(() => {
        let insertDocument;

        insertDocument = function() {
          let queries = Promise.resolve();

          doc.regions.forEach(region => {
            let regionId;

            if (!regionIds.hasOwnProperty(region)) {
              throw new Error(
                  `Region "${region}" not found, inserting document ${doc.name}`
              );
            }
            regionId = regionIds[region];

            queries = queries.then(
                _this.db.query(
                    'INSERT INTO document (region_id,name) VALUES ($1, $2)',
                    [regionId, doc.name]
                )
            );
          });

          return queries;
        };

        if (_this.mode === MODE_SILENT) {
          return insertDocument();
        }

        return _this.db
          .query('SELECT id FROM document WHERE name=$1', [doc.name])
          .then(result => {
            if (result.rows.length == 0) {
              // document does not exist
              return insertDocument();
            }

            // found existing document
            let documentId, skipInsertDocument;

            documentId = result.rows[0].id;
            skipInsertDocument = function() {
              // nothing to do here
            };

            if (_this.mode === MODE_MISSING) {
              // document already exists
              return skipInsertDocument();
            } else {
              // ask user whether to remove existing data
              let prompt = inquirer.createPromptModule();
              return prompt([
                {
                  name: 'dropDocument',
                  type: 'confirm',
                  message: `Document ${
                    doc.name
                  } already exists, drop and reload document`,
                  default: false
                }
              ]).then(answers => {
                if (answers.dropDocument) {
                  return _this.db
                    .query('DELETE FROM document WHERE id=$1', [documentId])
                    .then(() => {
                      return insertDocument();
                    });
                } else {
                  return skipInsertDocument();
                }
              });
            }
          });
      });
    });

    return promise;
  };

  /**
   * Insert region data.
   *
   * Using options.regions.
   *
   * @return {Promise}
   *     promise representing that all region data has been inserted.
   */
  _this._insertData = function(regionIds) {
    let promise;

    promise = Promise.resolve();

    _this.regions.forEach(region => {
      promise = promise.then(() => {
        return _this
          ._shouldInsertData(region, regionIds)
          .then(shouldInsertData => {
            if (shouldInsertData) {
              process.stderr.write('Loading ' + region.name + ' region data\n');
              return _this.griddedInserter.insertGrid(region, regionIds);
            } else {
              process.stderr.write(
                  `Region "${region.name}" data already loaded\n`
              );
            }
          })
          .catch(err => {
            process.stderr.write(
                `Failed loading data for region "${region.name}"\n${err.stack}\n`
            );
          });
      });
    });

    return promise;
  };

  /**
   * Create indexes.
   *
   * @return {Promise}
   *     promise representing that all indexes have been created.
   */
  _this._createIndexes = function() {
    return dbUtils.readSqlFile(_this.indexFile).then(statements => {
      return dbUtils.exec(_this.db, statements);
    });
  };

  /**
   * Checks if the `data` table already has data for the given `region`.
   *
   * @param region {Object}
   *      The region for which to check if data already exists
   * @param regionIds {Object}
   *      A mapping of region names -> existing region ids
   *
   * @return Promise<Boolean>
   *      A promise resolving with `true` if the `data` table already has data
   *      for the given region. Resolves with `false` otherwise. Promise rejects
   *      only if an error occurred.
   */
  _this._hasData = function(region, regionIds) {
    const sql = `
      SELECT min(region_id) as region_id
      FROM data
      WHERE region_id=$1
    `;

    return _this.db.query(sql, [regionIds[region.name]]).then(result => {
      const regionId = Number(result.rows[0].region_id);
      return regionId === regionIds[region.name];
    });
  };

  /**
   * Determines if the region data should be downloaded, read, and inserted.
   * This depends on a variety of factors including if data currently exist,
   * the current data load type, and an interactive reponse from the user.
   *
   * @param region {Object}
   *      The region for which to check if data already exists
   * @param regionIds {Object}
   *      A mapping of region names -> existing region ids
   *
   * @return Promise<Boolean>
   *      A promise resolving with `true` if the region data should be loaded.
   *      Resolves with `false` otherwise. Promise rejects only if an error
   *      occured.
   */
  _this._shouldInsertData = function(region, regionIds) {
    return Promise.resolve().then(() => {
      if (_this.mode === MODE_SILENT) {
        return true;
      }

      return _this._hasData(region, regionIds).then(hasData => {
        if (!hasData) {
          return true;
        } else if (_this.mode === MODE_MISSING) {
          // Already have data and only loading missing
          return false;
        }

        const prompt = inquirer.createPromptModule();
        return prompt([
          {
            name: 'dropData',
            type: 'confirm',
            message: `Data for region ${
              region.name
            } already exists, drop and reload data`,
            default: false
          }
        ]).then(answers => {
          if (answers.dropData) {
            return _this.db
              .query('DELETE FROM data WHERE id=$1', [regionIds[region.name]])
              .then(() => true);
          } else {
            return false;
          }
        });
      });
    });
  };

  _this.destroy = function() {
    if (_this === null) {
      return;
    }

    _this = null;
  };

  /**
   * Run data loader using configured options.
   *
   * @return {Promise}
   *     promise representing that all data has been loaded.
   */
  _this.run = function() {
    let createIndexes, createSchema, insertData, insertDocuments, insertRegions;

    // set order of load operations
    createSchema = _this._createSchema();
    insertRegions = createSchema.then(_this._insertRegions);
    insertDocuments = insertRegions.then(_this._insertDocuments);
    insertData = insertDocuments.then(() => {
      // need region ids from insertRegions
      return insertRegions.then(_this._insertData);
    });
    createIndexes = insertData.then(_this._createIndexes);

    return createIndexes;
  };

  _initialize(options);
  options = null;
  return _this;
};

AbstractDataLoader.MODE_INTERACTIVE = MODE_INTERACTIVE;
AbstractDataLoader.MODE_MISSING = MODE_MISSING;
AbstractDataLoader.MODE_SILENT = MODE_SILENT;

module.exports = AbstractDataLoader;
