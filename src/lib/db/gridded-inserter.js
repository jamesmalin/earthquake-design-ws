'use strict';

const copyFrom = require('pg-copy-streams').from,
    { PassThrough } = require('stream'),
    zlib = require('zlib');

const DbUtils = require('./db-utils'),
    TransformDataStream = require('../util/transform-data-stream'),
    UrlStream = require('../util/url-stream');

const GriddedInserter = function(options) {
  let _this, _initialize;

  _this = {};

  _initialize = function(options) {
    options = options || {};

    _this.formats = options.formats;
    _this.dataColumns = options.dataColumns;
    _this.dataLoadOpts = options.dataLoadOpts;
    _this.db = options.db;
  };

  _this.getTransformStream = function(format) {
    const formatOptions = _this.formats[format];

    if (!formatOptions || formatOptions.passThrough) {
      return new PassThrough();
    } else {
      return TransformDataStream(formatOptions);
    }
  };

  _this.insertGrid = function(region, regionIds) {
    if (region.data.type === 'split') {
      return _this.insertSplitGrids(region, regionIds);
    } else {
      return _this.insertCombinedGrids(region, regionIds);
    }
  };

  _this.insertTempTable = function(metadata, salt, tableDef, transformStream) {
    const tableName = `temp_region_data_${salt}`;
    tableDef = tableDef || ['LIKE data'];

    return DbUtils.exec(_this.db, [
      `DROP TABLE IF EXISTS ${tableName} CASCADE`,
      `CREATE TABLE ${tableName} (${tableDef.join(',')})`
    ]).then(() => {
      return new Promise((resolve, reject) => {
        const downloadStream = UrlStream({ url: metadata.url });
        const gunzipStream = zlib.createGunzip();
        const insertStream = _this.db.query(
            copyFrom(`COPY ${tableName} FROM STDIN ${_this.dataLoadOpts}`)
        );

        const doReject = error => {
          downloadStream.destroy();
          return reject(error);
        };
        const doResolve = () => {
          downloadStream.destroy();
          return resolve(tableName);
        };

        downloadStream.on('error', doReject);
        insertStream.on('error', doReject);
        insertStream.on('end', doResolve);

        downloadStream
          .pipe(gunzipStream)
          .pipe(transformStream)
          .pipe(insertStream);
      });
    });
  };

  _this.insertSplitGrids = function(region, regionIds) {
    let scalarTable;
    const scalarFormat = region.data.scalar.format;
    const scalarTransform = _this.getTransformStream(scalarFormat);
    const scalarTableDef = _this.formats[scalarFormat].tableDef;

    let vectorTable;
    const vectorFormat = region.data.vector.format;
    const vectorTransform = _this.getTransformStream(vectorFormat);
    const vectorTableDef = _this.formats[vectorFormat].tableDef;

    return Promise.all([
      _this.insertTempTable(
          region.data.scalar,
          'scalar',
          scalarTableDef,
          scalarTransform
      ),
      _this.insertTempTable(
          region.data.vector,
          'vector',
          vectorTableDef,
          vectorTransform
      )
    ])
      .then(tableNames => {
        scalarTable = tableNames[0];
        vectorTable = tableNames[1];

        const sql = `INSERT INTO data
          (region_id, ${_this.dataColumns.join(', ')})
          (
            SELECT
              ${regionIds[region.name]},
              scalar.latitude,
              scalar.longitude,
              scalar.value,
              vector.value
            FROM
              ${scalarTable} AS scalar,
              ${vectorTable} AS vector
            WHERE
              scalar.latitude = vector.latitude AND
              scalar.longitude = vector.longitude
          )
        `;

        return _this.db.query(sql);
      })
      .then(() => {
        // remove temporary tables
        return DbUtils.exec(_this.db, [
          `DROP TABLE ${scalarTable} CASCADE`,
          `DROP TABLE ${vectorTable} CASCADE`
        ]);
      });
  };

  _this.insertCombinedGrids = function(region, regionIds) {
    let tableName;
    const format = region.data.format;
    const transform = _this.getTransformStream(format);
    const tableDef = _this.formats[format].tableDef;

    return _this
      .insertTempTable(region.data, 'combined', tableDef, transform)
      .then(name => {
        tableName = name;

        const sql = `
        INSERT INTO data (region_id, ${_this.dataColumns.join(', ')}) (
          SELECT
            ${regionIds[region.name]},
            *
          FROM
            ${tableName}
        )
      `;

        return _this.db.query(sql);
      })
      .then(() => {
        // remove temporary table
        return _this.db.query(`DROP TABLE ${tableName} CASCADE`);
      });
  };

  _initialize(options);
  options = null;
  return _this;
};

module.exports = GriddedInserter;
