'use strict';

const NumberUtils = require('../../util/number-utils').instance;

const _METADATA = {
  'ASCE7-05': [
    {
      regions: [
        'AK0P10_760',
        'CANV0P01_760',
        'CEUS0P01_760',
        'HI0P02_760',
        'PACNW0P01_760',
        'PRVI0P05_760',
        'SLC0P01_760',
        'COUS0P05_760'
      ],
      data: {
        modelVersion: 'v2.0.x',
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'ASCE7-10': [
    {
      regions: ['AK0P05_760', 'COUS0P01_760', 'PRVI0P01_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    {
      regions: ['HI0P02_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'ASCE41-13': [
    {
      regions: ['COUS0P01_760', 'AK0P05_760', 'PRVI0P01_760'],
      data: {
        curveInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR,
        modelVersion: 'v3.1.x',
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    {
      regions: ['HI0P02_760'],
      data: {
        curveInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR,
        modelVersion: 'v3.1.x',
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'ASCE7-16': [
    // LogY interpolation, standard factors
    {
      regions: ['AMSAM0P10_760', 'COUS0P01_760', 'GNMI0P10_760'],
      data: {
        modelVersion: 'v4.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LINEARX_LOGY_LINEAR
      }
    },
    // LinearY interpolation, standard factors
    {
      regions: ['AK0P05_760', 'PRVI0P01_760'],
      data: {
        modelVersion: 'v4.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    // LinearY interpolation, custom factors
    {
      regions: ['HI0P02_760'],
      data: {
        modelVersion: 'v4.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'ASCE41-17': [
    {
      regions: [
        'AK0P05_760',
        'AMSAM0P10_760',
        'COUS0P01_760',
        'GNMI0P10_760',
        'PRVI0P01_760'
      ],
      data: {
        curveInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR,
        modelVersion: 'v4.0.x',
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LINEARX_LOGY_LINEAR
      }
    },
    {
      regions: ['HI0P02_760'],
      data: {
        curveInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR,
        modelVersion: 'v4.0.x',
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LINEARX_LOGY_LINEAR
      }
    }
  ],

  'IBC-2012': [
    {
      regions: ['AK0P05_760', 'COUS0P01_760', 'PRVI0P01_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.6,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    {
      regions: ['HI0P02_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.6,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'IBC-2015': [
    {
      regions: [
        'AK0P05_760',
        'AMSAM0P10_760',
        'COUS0P01_760',
        'GNMI0P10_760',
        'PRVI0P01_760'
      ],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.6,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    {
      regions: ['HI0P02_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.6,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'NEHRP-2009': [
    {
      regions: ['AK0P05_760', 'COUS0P01_760', 'PRVI0P01_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.6,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    {
      regions: ['HI0P02_760'],
      data: {
        modelVersion: 'v3.1.x',
        pgadFloor: 0.6,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],

  'NEHRP-2015': [
    // LogY interpolation, standard factors
    {
      regions: ['AMSAM0P10_760', 'COUS0P01_760', 'GNMI0P10_760'],
      data: {
        modelVersion: 'v4.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LINEARX_LOGY_LINEAR
      }
    },
    // LinearY interpolation, standard factors
    {
      regions: ['AK0P05_760', 'PRVI0P01_760'],
      data: {
        modelVersion: 'v4.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.1, 1.3],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    },
    // LinearY interpolation, custom factors
    {
      regions: ['HI0P02_760'],
      data: {
        modelVersion: 'v4.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.8,
        sadFloor: [1.5, 0.6],
        sadPercentileFactor: [1.8, 1.8],
        sauhMaxDirectionFactor: [1.0, 1.0],
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ],
  'NEHRP-2020': [
    {
      regions: ['COUS0P50_2000'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.37,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.5,
          0.5,
          0.52,
          0.6,
          0.81,
          1.04,
          1.12,
          1.12,
          1.01,
          0.9,
          0.81,
          0.69,
          0.6,
          0.46,
          0.37,
          0.26,
          0.21,
          0.15,
          0.12,
          0.1,
          0.06,
          0.04
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_1080'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.43,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.57,
          0.57,
          0.58,
          0.66,
          0.89,
          1.14,
          1.25,
          1.29,
          1.19,
          1.07,
          0.98,
          0.83,
          0.72,
          0.54,
          0.42,
          0.29,
          0.23,
          0.17,
          0.13,
          0.11,
          0.07,
          0.05
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_760'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.5,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.66,
          0.66,
          0.68,
          0.75,
          0.95,
          1.21,
          1.37,
          1.53,
          1.5,
          1.4,
          1.3,
          1.14,
          1.01,
          0.76,
          0.6,
          0.41,
          0.31,
          0.21,
          0.16,
          0.13,
          0.08,
          0.05
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_530'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.55,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.73,
          0.73,
          0.74,
          0.79,
          0.96,
          1.19,
          1.37,
          1.61,
          1.71,
          1.71,
          1.66,
          1.53,
          1.38,
          1.07,
          0.86,
          0.6,
          0.45,
          0.31,
          0.24,
          0.19,
          0.11,
          0.07
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_365'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.56,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.74,
          0.75,
          0.75,
          0.78,
          0.89,
          1.08,
          1.24,
          1.5,
          1.66,
          1.77,
          1.83,
          1.82,
          1.73,
          1.41,
          1.17,
          0.84,
          0.64,
          0.45,
          0.34,
          0.26,
          0.15,
          0.09
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_260'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.53,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.69,
          0.7,
          0.7,
          0.7,
          0.76,
          0.9,
          1.04,
          1.27,
          1.44,
          1.58,
          1.71,
          1.8,
          1.8,
          1.57,
          1.39,
          1.09,
          0.88,
          0.63,
          0.47,
          0.36,
          0.19,
          0.11
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_185'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.46,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.61,
          0.62,
          0.62,
          0.62,
          0.62,
          0.71,
          0.82,
          1.0,
          1.15,
          1.3,
          1.44,
          1.61,
          1.68,
          1.6,
          1.51,
          1.35,
          1.19,
          0.89,
          0.66,
          0.49,
          0.26,
          0.14
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    },
    {
      regions: ['COUS0P50_150'],
      data: {
        modelVersion: 'v5.0.x',
        pgadFloor: 0.42,
        pgadPercentileFactor: 1.0,
        sadFloor: [
          0.55,
          0.55,
          0.55,
          0.55,
          0.55,
          0.62,
          0.72,
          0.87,
          1.01,
          1.15,
          1.3,
          1.48,
          1.6,
          1.59,
          1.58,
          1.54,
          1.46,
          1.11,
          0.81,
          0.61,
          0.31,
          0.17
        ],
        sadPercentileFactor: [
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0,
          1.0
        ],
        sauhMaxDirectionFactor: [
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.2,
          1.203,
          1.206,
          1.213,
          1.219,
          1.234,
          1.25,
          1.253,
          1.256,
          1.261,
          1.267,
          1.272,
          1.286,
          1.3
        ],
        spatialInterpolationMethod: NumberUtils.INTERPOLATE_LOGX_LOGY_LINEAR
      }
    }
  ],

  'AASHTO-2009': [
    {
      regions: ['AK0P10_760', 'COUS0P05_760', 'HI0P02_760', 'PRVI0P05_760'],
      data: {
        modelVersion: 'v3.1.x',
        spatialInterpolationMethod:
          NumberUtils.INTERPOLATE_LINEARX_LINEARY_LINEAR
      }
    }
  ]
};

module.exports = _METADATA;
