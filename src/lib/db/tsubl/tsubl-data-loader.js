'use strict';

const AbstractDataLoader = require('../abstract-data-loader'),
    Config = require('../../util/config'),
    extend = require('extend');

let config = Config().get(),
    documents = require('./documents.json'),
    regions = require('./regions.json');

const _DEFAULTS = {
  dataColumns: ['value', 'shape'],
  db: null,
  documents: documents,
  formats: [
    {
      dataColumns: ['value', 'shape'],
      csvColumns: ['value', 'shape'],
      passThrough: true,
      saValues: [],
      tableDef: ['value INTEGER', 'shape geography(Geometry,4326)']
    }
  ],

  indexFile: __dirname + '/./index.sql',
  mode: AbstractDataLoader.MODE_MISSING,
  regions: regions,
  schemaFile: __dirname + '/./schema.sql',
  schemaName: config.DB_SCHEMA_TSUBL,
  schemaUser: config.DB_USER,
  dataLoadOpts: 'WITH DELIMITER \'|\' CSV HEADER'
};

const TSubLDataLoader = function(options) {
  let _this;

  options = extend({}, _DEFAULTS, options);
  _this = AbstractDataLoader(options);

  _this._getRegionInsertQuery = function() {
    return `
      INSERT INTO region (
        name,
        grid_spacing,
        max_latitude,
        max_longitude,
        min_latitude,
        min_longitude
      ) VALUES ($1, $2, $3, $4, $5, $6)
      RETURNING id
    `;
  };

  _this._getRegionInsertParams = function(region) {
    return [
      region.name,
      region.grid_spacing,
      region.max_latitude,
      region.max_longitude,
      region.min_latitude,
      region.min_longitude
    ];
  };

  options = null;
  return _this;
};

module.exports = TSubLDataLoader;
