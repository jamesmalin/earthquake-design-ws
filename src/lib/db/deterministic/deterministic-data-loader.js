'use strict';

const AbstractDataLoader = require('../abstract-data-loader'),
    Config = require('../../util/config'),
    extend = require('extend');

let config = Config().get(),
    documents = require('./documents.json'),
    regions = require('./regions.json');

const _DEFAULTS = {
  dataColumns: ['latitude', 'longitude', 'pgad', 'sad'],
  db: null,
  documents: documents,
  formats: [
    {
      csvColumns: [
        'LATITUDE',
        'LONGITUDE',
        'MAPPED_PGAD',
        'MAPPED_S1D',
        'MAPPED_SSD'
      ],
      saValues: ['MAPPED_SSD', 'MAPPED_S1D'],
      tableDef: [
        'latitude NUMERIC',
        'longitude NUMERIC',
        'scalar NUMERIC',
        'vector NUMERIC ARRAY'
      ]
    },
    {
      csvColumns: ['LATITUDE', 'LONGITUDE', 'PGAD'],
      saValues: [],
      tableDef: [
        'latitude NUMERIC',
        'longitude NUMERIC',
        'value NUMERIC',
        'ignore NUMERIC ARRAY'
      ]
    },
    {
      csvColumns: [
        'LATITUDE',
        'LONGITUDE',
        'SA0P0',
        'SA0P01',
        'SA0P02',
        'SA0P03',
        'SA0P05',
        'SA0P075',
        'SA0P1',
        'SA0P15',
        'SA0P2',
        'SA0P25',
        'SA0P3',
        'SA0P4',
        'SA0P5',
        'SA0P75',
        'SA1P0',
        'SA1P5',
        'SA2P0',
        'SA3P0',
        'SA4P0',
        'SA5P0',
        'SA7P5',
        'SA10P0'
      ],
      saValues: [
        'SA0P0',
        'SA0P01',
        'SA0P02',
        'SA0P03',
        'SA0P05',
        'SA0P075',
        'SA0P1',
        'SA0P15',
        'SA0P2',
        'SA0P25',
        'SA0P3',
        'SA0P4',
        'SA0P5',
        'SA0P75',
        'SA1P0',
        'SA1P5',
        'SA2P0',
        'SA3P0',
        'SA4P0',
        'SA5P0',
        'SA7P5',
        'SA10P0'
      ],
      tableDef: ['latitude NUMERIC', 'longitude NUMERIC', 'value NUMERIC ARRAY']
    }
  ],
  indexFile: __dirname + '/./index.sql',
  mode: AbstractDataLoader.MODE_MISSING,
  regions: regions,
  schemaFile: __dirname + '/./schema.sql',
  schemaName: config.DB_SCHEMA_DETERMINISTIC,
  schemaUser: config.DB_USER,
  dataLoadOpts: 'WITH CSV HEADER'
};

const DeterministicDataLoader = function(options) {
  let _this;

  options = extend({}, _DEFAULTS, options);
  _this = AbstractDataLoader(options);

  options = null;
  return _this;
};

module.exports = DeterministicDataLoader;
