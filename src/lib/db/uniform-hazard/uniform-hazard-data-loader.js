'use strict';

const AbstractDataLoader = require('../abstract-data-loader'),
    Config = require('../../util/config'),
    extend = require('extend');

let config = Config().get(),
    documents = require('./documents.json'),
    regions = require('./regions.json');

const _DEFAULTS = {
  dataColumns: ['latitude', 'longitude', 'pgauh', 'sauh'],
  dataLoadOpts: 'WITH CSV HEADER',
  db: null,
  documents: documents,
  formats: [
    {
      csvColumns: [
        'LATITUDE',
        'LONGITUDE',
        'MAPPED_PGA',
        'MAPPED_S1',
        'MAPPED_SS'
      ],
      saValues: ['MAPPED_SS', 'MAPPED_S1'],
      tableDef: [
        'latitude NUMERIC',
        'longitude NUMERIC',
        'pgauh NUMERIC',
        'sauh NUMERIC ARRAY'
      ]
    },
    {
      csvColumns: ['LATITUDE', 'LONGITUDE', 'PGAD'],
      saValues: [],
      tableDef: [
        'latitude NUMERIC',
        'longitude NUMERIC',
        'value NUMERIC',
        'ignore NUMERIC ARRAY'
      ]
    },
    {
      csvColumns: [
        'LATITUDE',
        'LONGITUDE',
        'SA0P0',
        'SA0P01',
        'SA0P02',
        'SA0P03',
        'SA0P05',
        'SA0P075',
        'SA0P1',
        'SA0P15',
        'SA0P2',
        'SA0P25',
        'SA0P3',
        'SA0P4',
        'SA0P5',
        'SA0P75',
        'SA1P0',
        'SA1P5',
        'SA2P0',
        'SA3P0',
        'SA4P0',
        'SA5P0',
        'SA7P5',
        'SA10P0'
      ],
      saValues: [
        'SA0P0',
        'SA0P01',
        'SA0P02',
        'SA0P03',
        'SA0P05',
        'SA0P075',
        'SA0P1',
        'SA0P15',
        'SA0P2',
        'SA0P25',
        'SA0P3',
        'SA0P4',
        'SA0P5',
        'SA0P75',
        'SA1P0',
        'SA1P5',
        'SA2P0',
        'SA3P0',
        'SA4P0',
        'SA5P0',
        'SA7P5',
        'SA10P0'
      ],
      tableDef: ['latitude NUMERIC', 'longitude NUMERIC', 'value NUMERIC ARRAY']
    }
  ],
  indexFile: __dirname + '/./index.sql',
  mode: AbstractDataLoader.MODE_MISSING,
  regions: regions,
  schemaFile: __dirname + '/./schema.sql',
  schemaName: config.DB_SCHEMA_UNIFORM_HAZARD,
  schemaUser: config.DB_USER
};

const UniformHazardDataLoader = function(options) {
  let _this;

  options = extend({}, _DEFAULTS, options);
  _this = AbstractDataLoader(options);

  options = null;
  return _this;
};

module.exports = UniformHazardDataLoader;
