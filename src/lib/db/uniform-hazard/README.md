# uniform_hazard database schema

## Tables

- `data`

  Uniform hazard mapped data values for specific points.

  Each data point is part of a `region`.

- `document`

  Metadata for specific design documents, which define how
  mapped data values are interpolated and processed to generate
  uniform hazard values.

  There may be many records in the document table for a given document,
  (identified by `name`)
  one for each region associated with the document.

- `region`

  Metadata for a grid of uniform hazard data values.

  A region may be referenced by multiple design documents.

## Reference Data

- `documents.json`

  An array of document to region mappings.

- `regions.json`

  An array of region metadata, with `url`s to corresponding CSV data files.

## Loading the schema

- make sure the project is configured

  `npm run postinstall`

- run the data loader

  `node src/lib/db/load_data.js --silent --data=uniform_hazard`
