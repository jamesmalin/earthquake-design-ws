'use strict';

const CommonFunctions = require('../util/common-functions'),
    NSHM_2002Factory = require('../basis/nshm_2002-factory'),
    extend = require('extend');

const _DEFAULTS = {
  referenceDocument: 'ASCE7-05'
};

/**
 * Class: ASCE7_05Factory
 *
 * @param options Object
 *     Configuration options for this instance.
 */
const ASCE7_05Factory = function(options) {
  let _this;

  options = extend({}, _DEFAULTS, options);
  _this = NSHM_2002Factory(options);

  /**
   * Computes Ss and S1 values from initial result `data` fetched from
   * the metadata- and uniformHazard- factories.
   *
   * @param data {Array}
   *     An array containing 1,2, or 4 gridded data point results ...
   * @param data.metadata {Object}
   *     An object containing metadata for the calculation
   * @param data.uniformHazard {Object}
   *     An object containing uniformHazard hazard data for the caluclation
   *
   * @return {Promise}
   *     A promise that will resolve with an object containing "ss" and "s1"
   *     keys with corresponding data.
   */
  _this.computeBasicDesign = function(data) {
    return new Promise((resolve, reject) => {
      let uniformHazard, result;

      try {
        result = {};
        uniformHazard = data.uniformHazard.response.data;

        result = {
          ss: CommonFunctions.getPeriodValue(uniformHazard.sauh, 0.2),
          s1: CommonFunctions.getPeriodValue(uniformHazard.sauh, 1.0)
        };

        resolve(result);
      } catch (err) {
        reject(err);
      }
    });
  };

  /**
   * Computes the final ground motion values applying site amplification
   * factors and design (2/3) weighting.
   *
   * @param data {Object}
   *     An object containing `basicDesign` and `siteAmplification` information.
   * @param data.basicDesign {Object}
   *     An object containing `ss` and `s1`ground motion values
   * @param data.siteAmplification {Object}
   *     An object containing `fa` and `fv` ground motion values
   *
   * @return {Object}
   *    An object containing the final design ground motion data, namely:
   *    `sms`, `sm1`, `sds`, `sd1`
   */
  _this.computeFinalDesign = function(data) {
    // return Promise.resolve({});
    return new Promise((resolve, reject) => {
      let basicDesign, finalDesign, siteAmplification;

      finalDesign = {
        sms: null,
        sm1: null,
        sds: null,
        sd1: null
      };

      try {
        basicDesign = data.basicDesign;
        siteAmplification = data.siteAmplification;

        finalDesign.sms = _this.computeSiteModifiedValue(
            basicDesign.ss,
            siteAmplification.fa
        );
        finalDesign.sm1 = _this.computeSiteModifiedValue(
            basicDesign.s1,
            siteAmplification.fv
        );

        finalDesign.sds = _this.computeDesignValue(finalDesign.sms);
        finalDesign.sd1 = _this.computeDesignValue(finalDesign.sm1);

        resolve(finalDesign);
      } catch (err) {
        reject(err);
      }
    });
  };

  _this.get = function(inputs) {
    let result;

    inputs = inputs || {};
    inputs.referenceDocument = _this.referenceDocument;

    result = {
      basicDesign: null,
      finalDesign: null,
      metadata: null,
      uniformHazard: null,
      siteAmplification: null,
      tSubL: null
    };

    // For most reference documents, only BC data are available for requests
    // to the component endpoints. The data are later amplified for desired
    // site conditions
    const componentInputs = JSON.parse(JSON.stringify(inputs)); // Deep copy
    componentInputs.siteClass = 'BC';

    return Promise.all([
      _this.uniformHazardService.getData(componentInputs),
      _this.metadataService.getData(componentInputs),
      _this.tSubLService.getData(componentInputs)
    ])
      .then(promiseResults => {
        result.uniformHazard = promiseResults[0];
        result.metadata = promiseResults[1];
        result.tSubL = promiseResults[2].response.data['tl'];
        result.inputs = inputs;

        return _this.computeBasicDesign(result);
      })
      .then(basicDesign => {
        result.basicDesign = basicDesign;

        return _this.siteAmplificationService.getData(
            extend(true, {}, inputs, basicDesign)
        );
      })
      .then(siteAmplification => {
        result.siteAmplification = siteAmplification.response.data;

        return _this.computeFinalDesign(result);
      })
      .then(finalDesign => {
        result.finalDesign = finalDesign;

        return Promise.all([
          _this.designCategoryFactory.getDesignCategory(
              inputs.riskCategory,
              result.basicDesign.s1,
              finalDesign.sds,
              finalDesign.sd1
          ),
          _this.computeSpectra(extend({ tSubL: result.tSubL }, finalDesign))
        ]);
      })
      .then(promiseResults => {
        result.designCategory = promiseResults[0];
        result.spectra = promiseResults[1];

        return result;
      });
  };

  options = null;
  return _this;
};

module.exports = ASCE7_05Factory;
