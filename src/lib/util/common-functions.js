'use strict';

const CommonFunctions = {
  getPeriodValue: function(data, period) {
    try {
      const periods = data.periods;
      const values = data.values;
      const index = periods.indexOf(period);

      if (index !== -1) {
        return values[index];
      } else {
        throw new Error('Requested period not found');
      }
    } catch (err) {
      return null;
    }
  }
};

module.exports = CommonFunctions;
