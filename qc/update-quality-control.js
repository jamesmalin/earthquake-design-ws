'use strict';

const QCRunner = require('./qc-runner.js');

let qcRunner;

qcRunner = QCRunner();

const qcTests = [
  {
    url: 'http://localhost:8000/ws/designmaps/aashto-2009.json',
    expected: require('./expectations/aashto-2009.expectation.json'),
    results: './results/aashto-2009.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/asce7-05.json',
    expected: require('./expectations/asce7-05.expectation.json'),
    results: './results/asce7-05.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/asce7-10.json',
    expected: require('./expectations/asce7-10.expectation.json'),
    results: './results/asce7-10.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/asce7-16.json',
    expected: require('./expectations/asce7-16.expectation.json'),
    results: './results/asce7-16.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/asce41-13.json',
    expected: require('./expectations/asce41-13.expectation.json'),
    results: './results/asce41-13.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/nehrp-2009.json',
    expected: require('./expectations/nehrp-2009.expectation.json'),
    results: './results/nehrp-2009.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/nehrp-2015.json',
    expected: require('./expectations/nehrp-2015.expectation.json'),
    results: './results/nehrp-2015.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/nehrp-2020.json',
    expected: require('./expectations/nehrp-2020.expectation.json'),
    results: './results/nehrp-2020.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/ibc-2012.json',
    expected: require('./expectations/ibc-2012.expectation.json'),
    results: './results/ibc-2012.qc.md'
  },
  {
    url: 'http://localhost:8000/ws/designmaps/ibc-2015.json',
    expected: require('./expectations/ibc-2015.expectation.json'),
    results: './results/ibc-2015.qc.md'
  }
];

Promise.resolve()
  .then(() => {
    // run qc tests one at a time, so the db is not overwhelmed
    // return all promises in results array, as if they were run using Promise.all
    const results = [];

    let promise = Promise.resolve();
    qcTests.forEach(item => {
      promise = promise.then(() => {
        const qcRun = qcRunner.run(item.url, item.expected, item.results);
        results.push(qcRun);
        return qcRun;
      });
    });

    return promise.then(() => {
      return results;
    });
  }).then(results => {
    return Promise.all(results);
  })
  .then(results => {
    let status;

    status = 0;

    results.forEach(result => {
      process.stdout.write(JSON.stringify(result) + '\n');
      status += result.fail;
    });

    process.stdout.write('\n');
    process.exit(status);
  })
  .catch(err => {
    if (err.stack) {
      process.stderr.write('' + err.stack);
    }
    process.exit(-1);
  });
