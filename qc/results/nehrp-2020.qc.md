# NEHRP-2020 Quality Control Tests +/- 0.0001
> Generated: Fri, 19 Apr 2019 17:09:00 GMT
> Using web service: http://localhost:8000/ws/designmaps/nehrp-2020.json

### Legend
 - :green_heart: Passing
 - :broken_heart: Failing


## Irvine (33.65, -117.8) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.866,"sm1":0.333,"pgam":0.404}

## Charleston (32.8, -79.95) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## Charleston (32.8, -79.95) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Boise (43.6, -116.2) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.193,"sm1":0.06,"pgam":0.37}

## Boise (43.6, -116.2) Site Class C, Risk Category: I
 - :green_heart: {"sms":0.368,"sm1":0.145,"pgam":0.55}

## Century City (34.05, -118.4) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.838,"pgam":0.907}

## Charleston (32.8, -79.95) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Everett (48, -122.2) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":2.011,"pgam":0.619}

## Charleston (32.8, -79.95) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Boise (43.6, -116.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.493,"sm1":0.277,"pgam":0.53}

## Boise (43.6, -116.2) Site Class B, Risk Category: I
 - :green_heart: {"sms":0.235,"sm1":0.07,"pgam":0.43}

## Chicago (41.8504, -87.6503) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Chicago (41.85, -87.65) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## New York (40.75, -74) Site Class E, Risk Category: I
 - :green_heart: {"sms":0.283,"sm1":0.105,"pgam":0.42}

## Memphis (35.1496, -90.0488) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Vallejo (38.1041, -122.2555) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## St. Louis (38.6, -90.2) Site Class B, Risk Category: I
 - :green_heart: {"sms":0.495,"sm1":0.187,"pgam":0.43}

## Oakland (37.8, -122.25) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## Seattle (47.6, -122.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.777,"pgam":0.81}

## Tacoma (47.25, -122.45) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## San Diego (32.7, -117.15) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## San Luis Obispo (35.3, -120.65) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Irvine (33.65, -117.8) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.536,"sm1":0.657,"pgam":0.618}

## Santa Rosa (38.45, -122.7) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.886}

## Ventura (34.3, -119.3) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.699}

## San Francisco (37.7752, -122.4185) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Santa Rosa (38.45, -122.7) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.775}

## Los Angeles (34.05, -118.25) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Charleston (32.8, -79.95) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Santa Rosa (38.45, -122.7) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":1.028}

## Vallejo (38.1, -122.25) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Seattle (47.6, -122.3) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.762}

## Charleston (32.7763, -79.9307) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Concord (37.95, -122) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":1.015}

## Long Beach (33.7669, -118.1883) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Los Angeles (34.05, -118.25) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## San Francisco (37.75, -122.4) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.843}

## Century City (34.05, -118.4) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.983}

## Monterey (36.6, -121.9) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.068,"sm1":0.343,"pgam":0.497}

## San Jose (37.35, -121.9) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.65}

## Seattle (47.6, -122.3) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.623}

## Boise (43.6, -116.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.493,"sm1":0.277,"pgam":0.53}

## Irvine (33.65, -117.8) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.032,"sm1":0.359,"pgam":0.464}

## Everett (48, -122.2) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.052,"sm1":0.378,"pgam":0.477}

## Concord (37.9779, -122.0301) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Concord (37.95, -122) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.782}

## Memphis (35.15, -90.05) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## New York (40.75, -74) Site Class B, Risk Category: I
 - :green_heart: {"sms":0.218,"sm1":0.05,"pgam":0.43}

## San Jose (37.35, -121.9) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.966}

## Century City (34.05, -118.4) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.674}

## Boise (43.6, -116.2) Site Class E, Risk Category: I
 - :green_heart: {"sms":0.614,"sm1":0.489,"pgam":0.42}

## Everett (48, -122.2) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.493,"sm1":0.68,"pgam":0.604}

## Ventura (34.3, -119.3) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.994}

## Chicago (41.85, -87.65) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Las Vegas (36.2, -115.15) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Santa Cruz (36.95, -122.05) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Vallejo (38.1, -122.25) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Tacoma (47.25, -122.45) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Long Beach (33.8, -118.2) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.828}

## Everett (48, -122.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.582,"sm1":1.227,"pgam":0.647}

## Chicago (41.85, -87.65) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Portland (45.5, -122.65) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Chicago (41.85, -87.65) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## San Jose (37.35, -121.9) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.99}

## Oakland (37.8, -122.25) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Concord (37.95, -122) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":1.015}

## Sacramento (38.6, -121.5) Site Class C, Risk Category: I
 - :green_heart: {"sms":0.725,"sm1":0.352,"pgam":0.55}

## Santa Rosa (38.45, -122.7) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":1.14}

## Concord (37.95, -122) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.865}

## Oakland (37.8, -122.25) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## St. Louis (38.6274, -90.1978) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Century City (34.05, -118.4) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.096,"pgam":0.716}

## Long Beach (33.8, -118.2) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.546}

## San Jose (37.35, -121.9) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.788}

## Oakland (37.8, -122.25) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Everett (48, -122.2) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.817,"sm1":0.29,"pgam":0.404}

## Reno (39.55, -119.8) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.831,"pgam":0.782}

## Las Vegas (36.2, -115.15) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## Northridge (34.2, -118.55) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Santa Cruz (36.95, -122.05) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Everett (47.9792, -122.2009) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Santa Cruz (36.95, -122.05) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Santa Cruz (36.9741, -122.0297) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Santa Barbara (34.45, -119.7) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.839,"pgam":0.893}

## Century City (34.05, -118.4) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.776}

## Concord (37.95, -122) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.75}

## Century City (34.05, -118.4) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.838,"pgam":0.907}

## San Bernardino (34.1, -117.3) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.837}

## San Francisco (37.75, -122.4) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.843}

## Sacramento (38.6, -121.5) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.975,"sm1":0.703,"pgam":0.53}

## San Mateo (37.55, -122.3) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.699}

## Concord (37.95, -122) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":1.116}

## Santa Barbara (34.45, -119.7) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.995}

## Santa Barbara (34.4209, -119.6973) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Charleston (32.8, -79.95) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Memphis (35.15, -90.05) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Sacramento (38.6, -121.5) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.215,"sm1":1.251,"pgam":0.42}

## San Jose (37.35, -121.9) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.966}

## San Luis Obispo (35.3, -120.65) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Riverside (33.95, -117.4) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.635,"pgam":0.781}

## Ventura (34.3, -119.3) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.678}

## Irvine (33.65, -117.8) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":1.971,"pgam":0.563}

## Oakland (37.8, -122.25) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Memphis (35.15, -90.05) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## St. Louis (38.6, -90.2) Site Class C, Risk Category: I
 - :green_heart: {"sms":0.628,"sm1":0.248,"pgam":0.55}

## Sacramento (38.5817, -121.4934) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Long Beach (33.8, -118.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.745,"pgam":0.792}

## Monterey (36.6, -121.9) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.898,"sm1":0.303,"pgam":0.436}

## New York (40.7143, -74.006) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Luis Obispo (35.2828, -120.6586) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Las Vegas (36.2, -115.15) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Northridge (34.2, -118.55) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## Northridge (34.2, -118.55) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Tacoma (47.25, -122.45) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Northridge (34.2, -118.55) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Ventura (34.2788, -119.2922) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Portland (45.5, -122.65) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Los Angeles (34.05, -118.25) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Riverside (33.95, -117.4) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":2.889,"pgam":0.679}

## Las Vegas (36.2, -115.15) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Seattle (47.6, -122.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.777,"pgam":0.81}

## San Francisco (37.75, -122.4) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.581}

## Santa Barbara (34.45, -119.7) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.703}

## San Diego (32.7153, -117.1564) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## New York (40.75, -74) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.216,"sm1":0.05,"pgam":0.37}

## Santa Cruz (36.95, -122.05) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Salt Lake City (40.75, -111.9) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.622,"sm1":1.338,"pgam":0.687}

## San Bernardino (34.1, -117.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":1.093}

## San Francisco (37.75, -122.4) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":0.86}

## Vallejo (38.1, -122.25) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## St. Louis (38.6, -90.2) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.488,"sm1":0.19,"pgam":0.37}

## Los Angeles (34.05, -118.25) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## San Mateo (37.55, -122.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.951}

## Century City (34.0498, -118.4252) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Reno (39.5298, -119.8128) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Portland (45.5, -122.65) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Ventura (34.3, -119.3) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.775}

## Santa Rosa (38.4406, -122.7133) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## New York (40.75, -74) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.308,"sm1":0.107,"pgam":0.53}

## Portland (45.5, -122.65) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## San Luis Obispo (35.3, -120.65) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Tacoma (47.25, -122.45) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Ventura (34.3, -119.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.903}

## Salt Lake City (40.7609, -111.8905) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Bernardino (34.1083, -117.2889) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Bernardino (34.1, -117.3) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.828}

## Long Beach (33.8, -118.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.745,"pgam":0.792}

## Northridge (34.2, -118.55) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Irvine (33.6695, -117.8223) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Seattle (47.6, -122.3) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":2.918,"pgam":0.779}

## Reno (39.55, -119.8) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.345,"pgam":0.538}

## St. Louis (38.6, -90.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.669,"sm1":0.392,"pgam":0.53}

## Monterey (36.6, -121.9) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":2.276,"pgam":0.574}

## St. Louis (38.6, -90.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.669,"sm1":0.392,"pgam":0.53}

## Sacramento (38.6, -121.5) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.975,"sm1":0.703,"pgam":0.53}

## San Bernardino (34.1, -117.3) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":1.199}

## Santa Barbara (34.45, -119.7) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.807}

## Reno (39.55, -119.8) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.402,"pgam":0.619}

## New York (40.75, -74) Site Class C, Risk Category: I
 - :green_heart: {"sms":0.279,"sm1":0.065,"pgam":0.55}

## Ventura (34.3, -119.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.903}

## Riverside (33.95, -117.4) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.849,"pgam":0.768}

## San Diego (32.7, -117.15) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Vallejo (38.1, -122.25) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Oakland (37.8044, -122.2698) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Salt Lake City (40.75, -111.9) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.505}

## San Diego (32.7, -117.15) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## San Bernardino (34.1, -117.3) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.957}

## Long Beach (33.8, -118.2) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":2.92,"pgam":0.65}

## Los Angeles (34.05, -118.25) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Riverside (33.9533, -117.3953) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Boise (43.6136, -116.2025) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Vallejo (38.1, -122.25) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Los Angeles (34.05, -118.25) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Memphis (35.15, -90.05) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Santa Cruz (36.95, -122.05) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## Los Angeles (34.0531, -118.2428) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Long Beach (33.8, -118.2) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.633}

## Reno (39.55, -119.8) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.428,"pgam":0.745}

## Salt Lake City (40.75, -111.9) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.805,"pgam":0.748}

## Vallejo (38.1, -122.25) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Monterey (36.6, -121.9) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.3,"pgam":0.662}

## Northridge (34.2, -118.55) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Portland (45.5236, -122.675) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Diego (32.7, -117.15) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Monterey (36.6, -121.9) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.3,"pgam":0.662}

## Everett (48, -122.2) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.582,"sm1":1.227,"pgam":0.647}

## San Luis Obispo (35.3, -120.65) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## San Diego (32.7, -117.15) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Santa Barbara (34.45, -119.7) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.127,"pgam":0.686}

## San Mateo (37.55, -122.3) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.742}

## New York (40.75, -74) Site Class D, Risk Category: I
 - :green_heart: {"sms":0.308,"sm1":0.107,"pgam":0.53}

## Salt Lake City (40.75, -111.9) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.425,"sm1":2.229,"pgam":0.531}

## San Diego (32.7, -117.15) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Reno (39.55, -119.8) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.428,"pgam":0.745}

## Tacoma (47.253, -122.4432) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Portland (45.5, -122.65) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Santa Rosa (38.45, -122.7) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.39,"pgam":0.767}

## Las Vegas (36.1751, -115.1365) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Riverside (33.95, -117.4) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.368,"pgam":0.511}

## Santa Cruz (36.95, -122.05) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Memphis (35.15, -90.05) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## St. Louis (38.6, -90.2) Site Class E, Risk Category: I
 - :green_heart: {"sms":0.611,"sm1":0.382,"pgam":0.42}

## Memphis (35.15, -90.05) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## San Francisco (37.75, -122.4) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.665}

## Salt Lake City (40.75, -111.9) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.622,"sm1":1.338,"pgam":0.687}

## Chicago (41.85, -87.65) Site Class B, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.43}

## Seattle (47.6, -122.3) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.909,"sm1":0.383,"pgam":0.52}

## San Luis Obispo (35.3, -120.65) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Santa Rosa (38.45, -122.7) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":1.028}

## Chicago (41.85, -87.65) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Irvine (33.65, -117.8) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.622,"sm1":1.17,"pgam":0.639}

## Las Vegas (36.2, -115.15) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Reno (39.55, -119.8) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":2.393,"pgam":0.619}

## Salt Lake City (40.75, -111.9) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.585}

## Las Vegas (36.2, -115.15) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Los Angeles (34.05, -118.25) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Mateo (37.55, -122.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":0.951}

## Tacoma (47.25, -122.45) Site Class D, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.53}

## Portland (45.5, -122.65) Site Class A, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.37}

## Northridge (34.2283, -118.5361) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Mateo (37.55, -122.3) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.9,"pgam":1.026}

## Monterey (36.6003, -121.8937) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Francisco (37.75, -122.4) Site Class E, Risk Category: I
 - :green_heart: {"sms":1.44,"sm1":3.333,"pgam":0.702}

## Riverside (33.95, -117.4) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.635,"pgam":0.781}

## Oakland (37.8, -122.25) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Sacramento (38.6, -121.5) Site Class A, Risk Category: I
 - :green_heart: {"sms":0.386,"sm1":0.15,"pgam":0.37}

## Irvine (33.65, -117.8) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.622,"sm1":1.17,"pgam":0.639}

## San Jose (37.35, -121.9) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.751}

## San Jose (37.3395, -121.8939) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## Riverside (33.95, -117.4) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.412,"pgam":0.588}

## San Bernardino (34.1, -117.3) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.896,"pgam":1.093}

## Monterey (36.6, -121.9) Site Class C, Risk Category: I
 - :green_heart: {"sms":1.543,"sm1":0.705,"pgam":0.653}

## Tacoma (47.25, -122.45) Site Class E, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.42}

## Santa Barbara (34.45, -119.7) Site Class D, Risk Category: I
 - :green_heart: {"sms":1.623,"sm1":1.839,"pgam":0.893}

## Seattle (47.6065, -122.3307) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Mateo (37.5631, -122.3245) Site Class BC, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.5}

## San Luis Obispo (35.3, -120.65) Site Class C, Risk Category: I
 - :green_heart: {"sms":0,"sm1":0,"pgam":0.55}

## Sacramento (38.6, -121.5) Site Class B, Risk Category: I
 - :green_heart: {"sms":0.464,"sm1":0.171,"pgam":0.43}

## San Mateo (37.55, -122.3) Site Class B, Risk Category: I
 - :green_heart: {"sms":1.078,"sm1":0.435,"pgam":0.806}


### Summary
 - :green_heart: 239 passing
 - :broken_heart: 0 failing
